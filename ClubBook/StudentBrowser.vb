﻿Public Class StudentBrowser
    Dim file As New FileAnalyzer
    Public selectedStudents As New ArrayList
    Dim textboxPlaceholder As String
    Dim infoTargetWidth As Integer
    Dim infoForcedWidth As Integer
    Dim startWidth As Integer
    Dim aniSpeed As Integer = 7
    Dim infoPanelShown As Boolean = False
    Dim picturesDirectory As String


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        textboxPlaceholder = TextBox1.Text
        Dim reader As New System.IO.StreamReader(My.Computer.FileSystem.CurrentDirectory + "/options.moe")
        Dim studentDataFileName As String = reader.ReadLine
        picturesDirectory = reader.ReadLine
        If Not picturesDirectory.Last = "/" Or Not picturesDirectory.Last = "\" Then
            picturesDirectory = picturesDirectory + "\"
        End If
        reader.Close()
        file.readFile(studentDataFileName)
        infoTargetWidth = Me.Width
        infoForcedWidth = infoTargetWidth
        Me.Size = New Point(690, 560)
        startWidth = 690

        lastLbl.Text = ""
        firstLbl.Text = ""
        genderLbl.Text = ""
        bornLbl.Text = ""
        numberLbl.Text = ""
        stuEmailLbl.Text = ""
        stuTeleLbl.Text = ""
        guardTeleLbl.Text = ""
        parentEmail1Lbl.Text = ""
        parentEmail2Lbl.Text = ""
        layoutListView()
    End Sub

    Public Sub layoutListView()


        ListView1.Clear()
        ListView1.CheckBoxes = True
        For Each catagorie In file.catagories
            ListView1.Columns.Add(catagorie.ToString)
            ListView1.Columns.Item(ListView1.Columns.Count - 1).Width = ListView1.Width / 4 - 6
            If ListView1.Columns.Count > 3 Then
                Exit For
            End If
        Next

        Dim currentIndex = ListView1.Items.Count
        Dim counter As Integer = 1
        For Each student As ArrayList In file.getTableviewData
            ListView1.Items.Add(student.Item(0))
            student.RemoveAt(0)
            counter = 1
            For Each info In student
                ListView1.Items.Item(currentIndex).SubItems.Add(info.ToString)
                counter = counter + 1
                If counter = 4 Then Exit For
            Next
            If selectedStudents.Contains(ListView1.Items(currentIndex).SubItems(3).Text) Then
                ListView1.Items(currentIndex).Checked = True
                selectedStudents.Add(ListView1.Items(currentIndex).SubItems(3).Text)
            End If
            currentIndex = currentIndex + 1
        Next
        If ListView1.Items.Count = 0 Then
            ListView1.Items.Add("No students to display.")
        End If
    End Sub

    Private Sub TextBox1_GotFocus(sender As Object, e As EventArgs) Handles TextBox1.GotFocus
        If TextBox1.Text = textboxPlaceholder Then
            TextBox1.Text = ""
            TextBox1.ForeColor = Color.Black
        End If

    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        searchTimer.Enabled = False
        searchTimer.Enabled = True

    End Sub

    Private Sub searchStudents()
        If TextBox1.Text = textboxPlaceholder Then Exit Sub
        If TextBox1.Text.Replace(" ", "") = "" Then
            Label1.Visible = True
            Label1.BringToFront()
            Me.Refresh()
            layoutListView()
            Label1.Visible = False
            Exit Sub
        End If


        ListView1.Items.Clear()
        ListView1.CheckBoxes = True
        Dim searchText As String = TextBox1.Text.ToLower
        Dim currentIndex As Integer = 0
        Dim counter As Integer = 0

        Label1.Visible = True
        Label1.BringToFront()
        Me.Refresh()
        For Each student As ArrayList In file.getTableviewData

            'If the first name, last name or student number match the search then add them to the listview
            If file.studentMatchesSearchString(student, searchText) Then
                ListView1.Items.Add(student.Item(0))
                student.RemoveAt(0)
                counter = 1
                For Each info In student
                    ListView1.Items.Item(currentIndex).SubItems.Add(info.ToString)
                    counter = counter + 1
                    If counter = 4 Then Exit For
                Next
                If selectedStudents.Contains(ListView1.Items(currentIndex).SubItems(3).Text) Then
                    ListView1.Items(currentIndex).Checked = True
                    selectedStudents.Add(ListView1.Items(currentIndex).SubItems(3).Text)
                End If
                currentIndex = currentIndex + 1
            End If
        Next
        Label1.Visible = False
        If ListView1.Items.Count = 0 Then
            ListView1.Items.Add("No matches.")
            ListView1.CheckBoxes = False
        End If

    End Sub

    Private Sub searchTimer_Tick(sender As Object, e As EventArgs) Handles searchTimer.Tick

        searchStudents()
        searchTimer.Enabled = False
    End Sub

    Private Sub ListView1_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles ListView1.ItemCheck

        If selectedStudents.Contains(ListView1.Items(e.Index).SubItems(3).Text) Then
            selectedStudents.Remove(ListView1.Items(e.Index).SubItems(3).Text)
        Else
            selectedStudents.Add(ListView1.Items(e.Index).SubItems(3).Text)
        End If

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub ShwToolStripMenuItem_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub infoPanelTimer_Tick(sender As Object, e As EventArgs) Handles infoPanelTimer.Tick
        Dim multiplier As Integer = 0
        If infoPanelShown Then
            multiplier = -1
        Else
            multiplier = 1
        End If

        Me.Width = Me.Width + multiplier * aniSpeed

        If Me.Width <= startWidth Then
            infoPanelTimer.Enabled = False
            infoPanelShown = False
            infoForcedWidth = infoTargetWidth

        ElseIf Me.Width >= infoForcedWidth Then
            infoPanelTimer.Enabled = False
            infoPanelShown = True
            showInfo()
           
        End If

    End Sub

    Private Sub ListView1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListView1.SelectedIndexChanged
        If ListView1.SelectedItems.Count > 0 And infoPanelShown Then
            showInfo()
        End If
    End Sub
    Private Sub showInfo()
        If ListView1.SelectedItems.Count = 0 Then Exit Sub
        Dim selectedItem As ListViewItem = ListView1.SelectedItems(0)

        
        Dim studentNumber As String = selectedItem.SubItems(3).Text
        lastLbl.Text = selectedItem.Text
        firstLbl.Text = selectedItem.SubItems(1).Text

        If selectedItem.SubItems(2).Text = "M" Then
            genderLbl.Text = "Male"
        Else
            genderLbl.Text = "Female"
        End If

        numberLbl.Text = studentNumber

        Dim infoList As New ArrayList
        infoList = file.numberDictionary(studentNumber).Clone
        Dim birthText As String = infoList(6).ToString.Substring(0, 4) + "/" + infoList(6).ToString.Substring(4, 2) + "/" + infoList(6).ToString.Substring(6, 2)
        Dim birthday As New DateTime(CInt(birthText.Split("/").GetValue(0)), CInt(birthText.Split("/").GetValue(1)), CInt(birthText.Split("/").GetValue(2)))
        bornLbl.Text = Math.Floor((DateTime.Now - birthday).Days / 365).ToString + "(" + birthText + ")"

        stuTeleLbl.Text = checkEmpty(infoList(7))
        guardTeleLbl.Text = checkEmpty(infoList(8))
        stuEmailLbl.Text = checkEmpty(infoList(9))

        parentEmail1Lbl.Text = ""
        parentEmail2Lbl.Text = ""

        'Adding the emails
        Dim rawEmails As String = checkEmpty(infoList(10))
        If rawEmails.Contains(";") Then
            'Formatting the first email(making it say Joe:joe@gmail instead of joe<joe@gmail>)
            Dim firstEmail As String = rawEmails.Split(";").GetValue(0).ToString.Replace("<", ": ")
            parentEmail1Lbl.Text = firstEmail.Substring(0, firstEmail.Count - 1)

            'Formatting the second email
            Dim secondEmail As String = rawEmails.Split(";").GetValue(1)
            If rawEmails.Length > firstEmail.Length + 1 And secondEmail.First = " " Then secondEmail = secondEmail.Substring(1)
            secondEmail = secondEmail.Replace("<", ": ")
            parentEmail2Lbl.Text = secondEmail.Substring(0, secondEmail.Count - 1)

        Else
            rawEmails = rawEmails.ToString.Replace("<", ": ")
            parentEmail1Lbl.Text = rawEmails.Substring(0, rawEmails.Count - 1)
        End If

        'Making the form wider if the email if too long for the screen
        While Me.Width - 20 < parentEmail1Lbl.Location.X + parentEmail1Lbl.Width Or Me.Width - 20 < parentEmail2Lbl.Location.X + parentEmail2Lbl.Width
            Me.Width = Me.Width + 1
        End While
        infoForcedWidth = Me.Width
        Dim imagePath As String = picturesDirectory + studentNumber + ".bmp"

        If My.Computer.FileSystem.FileExists(imagePath) = False Then
            imagePath = My.Computer.FileSystem.CurrentDirectory + "/NotFound.bmp"
        End If

        studentPicture.ImageLocation = imagePath
    End Sub
    Private Function checkEmpty(ByVal input As String) As String
        If input = "" Then
            input = "None"
        End If
        Return input
    End Function

    
   
    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked Then
            infoPanelTimer.Enabled = True
            'If ListView1.SelectedItems.Count > 0 And Not infoPanelShown Then
            '    showInfo()
            'End If
        Else
            infoPanelTimer.Enabled = True

        End If

    End Sub

    Private Sub firstLbl_Click(sender As Object, e As EventArgs) Handles firstLbl.Click
        My.Computer.Clipboard.SetText(firstLbl.Text)
        MsgBox("'" + firstLbl.Text + "' was copied to clipboard")
    End Sub

    Private Sub lastLbl_Click(sender As Object, e As EventArgs) Handles lastLbl.Click
        My.Computer.Clipboard.SetText(lastLbl.Text)
        MsgBox("'" + lastLbl.Text + "' was copied to clipboard")
    End Sub

    Private Sub genderLbl_Click(sender As Object, e As EventArgs) Handles genderLbl.Click

    End Sub

    Private Sub numberLbl_Click(sender As Object, e As EventArgs) Handles numberLbl.Click
        My.Computer.Clipboard.SetText(numberLbl.Text)
        MsgBox("'" + numberLbl.Text + "' was copied to clipboard")
    End Sub

    Private Sub stuTeleLbl_Click(sender As Object, e As EventArgs) Handles stuTeleLbl.Click
        My.Computer.Clipboard.SetText(stuTeleLbl.Text)
        MsgBox("'" + stuTeleLbl.Text + "' was copied to clipboard")
    End Sub

    Private Sub guardTeleLbl_Click(sender As Object, e As EventArgs) Handles guardTeleLbl.Click
        My.Computer.Clipboard.SetText(guardTeleLbl.Text)
        MsgBox("'" + guardTeleLbl.Text + "' was copied to clipboard")
    End Sub

    Private Sub stuEmailLbl_Click(sender As Object, e As EventArgs) Handles stuEmailLbl.Click
        If Not stuEmailLbl.Text = "None" Then
            My.Computer.Clipboard.SetText(stuEmailLbl.Text)
            MsgBox("'" + stuEmailLbl.Text + "' was copied to clipboard")
        End If
    End Sub

    Private Sub parentEmail1Lbl_Click(sender As Object, e As EventArgs) Handles parentEmail1Lbl.Click
        If Not parentEmail1Lbl.Text = "None" Then
            My.Computer.Clipboard.SetText(parentEmail1Lbl.Text.Split(":").GetValue(1).ToString.Substring(1))
            MsgBox("'" + parentEmail1Lbl.Text.Split(":").GetValue(1).ToString.Substring(1) + "' was copied to clipboard")
        End If
    End Sub

    Private Sub parentEmail2Lbl_Click(sender As Object, e As EventArgs) Handles parentEmail2Lbl.Click
        If Not parentEmail2Lbl.Text = "None" Then
            My.Computer.Clipboard.SetText(parentEmail2Lbl.Text.Split(":").GetValue(1).ToString.Substring(1))
            MsgBox("'" + parentEmail2Lbl.Text.Split(":").GetValue(1).ToString.Substring(1) + "' was copied to clipboard")
        End If
    End Sub
End Class
