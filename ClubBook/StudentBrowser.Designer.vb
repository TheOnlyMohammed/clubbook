﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StudentBrowser
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(StudentBrowser))
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.searchTimer = New System.Windows.Forms.Timer(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.studentPicture = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.firstLbl = New System.Windows.Forms.Label()
        Me.lastLbl = New System.Windows.Forms.Label()
        Me.genderLbl = New System.Windows.Forms.Label()
        Me.numberLbl = New System.Windows.Forms.Label()
        Me.stuTeleLbl = New System.Windows.Forms.Label()
        Me.guardTeleLbl = New System.Windows.Forms.Label()
        Me.stuEmailLbl = New System.Windows.Forms.Label()
        Me.infoPanelTimer = New System.Windows.Forms.Timer(Me.components)
        Me.bornLbl = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.parentEmail1Lbl = New System.Windows.Forms.Label()
        Me.parentEmail2Lbl = New System.Windows.Forms.Label()
        CType(Me.studentPicture, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ListView1
        '
        Me.ListView1.CheckBoxes = True
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.ListView1.FullRowSelect = True
        Me.ListView1.GridLines = True
        Me.ListView1.Location = New System.Drawing.Point(14, 58)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(647, 417)
        Me.ListView1.TabIndex = 0
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Width = 151
        '
        'TextBox1
        '
        Me.TextBox1.ForeColor = System.Drawing.Color.Gray
        Me.TextBox1.Location = New System.Drawing.Point(14, 12)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(647, 20)
        Me.TextBox1.TabIndex = 1
        Me.TextBox1.Text = "Search based on First Name, Last Name or Student Number"
        '
        'searchTimer
        '
        Me.searchTimer.Interval = 500
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(294, 245)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 21)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Loading..."
        Me.Label1.Visible = False
        '
        'Button1
        '
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.Button1.FlatAppearance.BorderSize = 2
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(545, 481)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(114, 33)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Add Students"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.Button2.FlatAppearance.BorderSize = 2
        Me.Button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon
        Me.Button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(12, 481)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(114, 33)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Cancel"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'studentPicture
        '
        Me.studentPicture.Image = CType(resources.GetObject("studentPicture.Image"), System.Drawing.Image)
        Me.studentPicture.Location = New System.Drawing.Point(689, 14)
        Me.studentPicture.Name = "studentPicture"
        Me.studentPicture.Size = New System.Drawing.Size(150, 190)
        Me.studentPicture.TabIndex = 6
        Me.studentPicture.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(686, 277)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 17)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Gender: "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(686, 247)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 17)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Last:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(686, 217)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 17)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "First: "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(686, 337)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(114, 17)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Student Number:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label6.Location = New System.Drawing.Point(686, 367)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(69, 17)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Phone #1:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label9.Location = New System.Drawing.Point(686, 307)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(37, 17)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "Age:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label7.Location = New System.Drawing.Point(686, 397)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(69, 17)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Phone #2:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label8.Location = New System.Drawing.Point(686, 427)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(100, 17)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Student Email:"
        '
        'firstLbl
        '
        Me.firstLbl.AutoSize = True
        Me.firstLbl.Location = New System.Drawing.Point(724, 221)
        Me.firstLbl.Name = "firstLbl"
        Me.firstLbl.Size = New System.Drawing.Size(30, 13)
        Me.firstLbl.TabIndex = 8
        Me.firstLbl.Text = "temp"
        '
        'lastLbl
        '
        Me.lastLbl.AutoSize = True
        Me.lastLbl.Location = New System.Drawing.Point(722, 250)
        Me.lastLbl.Name = "lastLbl"
        Me.lastLbl.Size = New System.Drawing.Size(30, 13)
        Me.lastLbl.TabIndex = 9
        Me.lastLbl.Text = "temp"
        '
        'genderLbl
        '
        Me.genderLbl.AutoSize = True
        Me.genderLbl.Location = New System.Drawing.Point(740, 280)
        Me.genderLbl.Name = "genderLbl"
        Me.genderLbl.Size = New System.Drawing.Size(30, 13)
        Me.genderLbl.TabIndex = 10
        Me.genderLbl.Text = "temp"
        '
        'numberLbl
        '
        Me.numberLbl.AutoSize = True
        Me.numberLbl.Location = New System.Drawing.Point(798, 340)
        Me.numberLbl.Name = "numberLbl"
        Me.numberLbl.Size = New System.Drawing.Size(30, 13)
        Me.numberLbl.TabIndex = 11
        Me.numberLbl.Text = "temp"
        '
        'stuTeleLbl
        '
        Me.stuTeleLbl.AutoSize = True
        Me.stuTeleLbl.Location = New System.Drawing.Point(753, 370)
        Me.stuTeleLbl.Name = "stuTeleLbl"
        Me.stuTeleLbl.Size = New System.Drawing.Size(30, 13)
        Me.stuTeleLbl.TabIndex = 12
        Me.stuTeleLbl.Text = "temp"
        '
        'guardTeleLbl
        '
        Me.guardTeleLbl.AutoSize = True
        Me.guardTeleLbl.Location = New System.Drawing.Point(754, 400)
        Me.guardTeleLbl.Name = "guardTeleLbl"
        Me.guardTeleLbl.Size = New System.Drawing.Size(30, 13)
        Me.guardTeleLbl.TabIndex = 13
        Me.guardTeleLbl.Text = "temp"
        '
        'stuEmailLbl
        '
        Me.stuEmailLbl.AutoSize = True
        Me.stuEmailLbl.Location = New System.Drawing.Point(784, 429)
        Me.stuEmailLbl.Name = "stuEmailLbl"
        Me.stuEmailLbl.Size = New System.Drawing.Size(30, 13)
        Me.stuEmailLbl.TabIndex = 14
        Me.stuEmailLbl.Text = "temp"
        '
        'infoPanelTimer
        '
        Me.infoPanelTimer.Interval = 10
        '
        'bornLbl
        '
        Me.bornLbl.AutoSize = True
        Me.bornLbl.Location = New System.Drawing.Point(722, 311)
        Me.bornLbl.Name = "bornLbl"
        Me.bornLbl.Size = New System.Drawing.Size(30, 13)
        Me.bornLbl.TabIndex = 16
        Me.bornLbl.Text = "temp"
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(14, 38)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(135, 17)
        Me.CheckBox1.TabIndex = 17
        Me.CheckBox1.Text = "Show More Information"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label10.Location = New System.Drawing.Point(686, 455)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(109, 17)
        Me.Label10.TabIndex = 7
        Me.Label10.Text = "Parent Email(s):"
        '
        'parentEmail1Lbl
        '
        Me.parentEmail1Lbl.AutoSize = True
        Me.parentEmail1Lbl.Location = New System.Drawing.Point(795, 457)
        Me.parentEmail1Lbl.Name = "parentEmail1Lbl"
        Me.parentEmail1Lbl.Size = New System.Drawing.Size(30, 13)
        Me.parentEmail1Lbl.TabIndex = 18
        Me.parentEmail1Lbl.Text = "temp"
        '
        'parentEmail2Lbl
        '
        Me.parentEmail2Lbl.AutoSize = True
        Me.parentEmail2Lbl.Location = New System.Drawing.Point(795, 472)
        Me.parentEmail2Lbl.Name = "parentEmail2Lbl"
        Me.parentEmail2Lbl.Size = New System.Drawing.Size(30, 13)
        Me.parentEmail2Lbl.TabIndex = 19
        Me.parentEmail2Lbl.Text = "temp"
        '
        'StudentBrowser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(966, 522)
        Me.Controls.Add(Me.parentEmail2Lbl)
        Me.Controls.Add(Me.parentEmail1Lbl)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.bornLbl)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.stuEmailLbl)
        Me.Controls.Add(Me.guardTeleLbl)
        Me.Controls.Add(Me.stuTeleLbl)
        Me.Controls.Add(Me.numberLbl)
        Me.Controls.Add(Me.genderLbl)
        Me.Controls.Add(Me.lastLbl)
        Me.Controls.Add(Me.firstLbl)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.studentPicture)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.ListView1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "StudentBrowser"
        Me.Text = "ClubBook"
        CType(Me.studentPicture, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents searchTimer As System.Windows.Forms.Timer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents studentPicture As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents firstLbl As System.Windows.Forms.Label
    Friend WithEvents lastLbl As System.Windows.Forms.Label
    Friend WithEvents genderLbl As System.Windows.Forms.Label
    Friend WithEvents numberLbl As System.Windows.Forms.Label
    Friend WithEvents stuTeleLbl As System.Windows.Forms.Label
    Friend WithEvents guardTeleLbl As System.Windows.Forms.Label
    Friend WithEvents stuEmailLbl As System.Windows.Forms.Label
    Friend WithEvents infoPanelTimer As System.Windows.Forms.Timer
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents bornLbl As System.Windows.Forms.Label
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents parentEmail1Lbl As System.Windows.Forms.Label
    Friend WithEvents parentEmail2Lbl As System.Windows.Forms.Label
    Private WithEvents ListView1 As System.Windows.Forms.ListView

End Class
