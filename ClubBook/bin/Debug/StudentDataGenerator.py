import random
alpha = ['q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m']
number = ['1','2','3','4','5','6','7','8','9','0']
endings = [',,,19970702,(905) 825-6776,(416) 507-7564,,Hanan Kassem<hanankassem@gmail.com>; Walid Esefan<w.esefan@gmail.com>,OHIP  2253818484HG  ,0001917200,CIA4UA01 CIE3MA01 ENG3UA02 FSF3UB03 MHF4UB02 SCH3UB03 SPH3UB02 SPH4UB01,,',',,,19970823,(905) 469-8086,(416) 212-7343,,Iryna Linkova<iryna@ilinkov.com>; Volodymyr Linkov<vlad@ilinkov.com>,"IEP on file,OHIP  3228417089XR  ",0001487440,BBB4MB01 BOH4MB01 ENG3UA02 FSF3UB02 MHF4UB02 SCH3UB01 SPH3UB02 SPH4UB02,,']

def generateName(length):
    returnString = ""
    for i in range(length):
        returnString+=alpha[random.randint(0,25)]
    return returnString+','


def generateNumber():
    returnString = ""
    for i in range(9):
        returnString+=number[random.randint(0,9)]
    return returnString+','

finalFile = "Last,First,Gender,Number,Program,HF1,HF2,DOB(YearMMDD),StudentTel#,GuardianTel#,StudentEmail,GuardianEmail,Miscel,SpecialUse,Courses,StaffNo1,staffNo2\n"

for i in range(2000):
    temp = generateName(random.randint(3,11)) + generateName(random.randint(3,7)) + 'M,' + generateNumber() + endings[random.randint(0,1)]
    finalFile+=temp+'\n'
    
f = open("AutoStudentFile.txt",'w')
f.write(finalFile)
f.close()
                        