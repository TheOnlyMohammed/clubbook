﻿
Public Class LoginScreen
    Dim expanded As Boolean = False
    Dim options As Boolean = False
    Dim analyzer As New FileAnalyzer



    Private Sub LoginScreen_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        refreshAccountList()

        'Initializing control locations
        Me.Size = New Point(275, 350)
        addBtn.Location = New Point(40, 273)
        deleteBtn.Location = New Point(deleteBtn.Location.X, addBtn.Location.Y + 40)
        renameBtn.Location = New Point(renameBtn.Location.X, addBtn.Location.Y + 40)
    End Sub

    Private Sub ListBox1_DoubleClick(sender As Object, e As EventArgs) Handles ListBox1.DoubleClick

        If ListBox1.SelectedItems.Count = 0 Then Exit Sub 'If nothing is selected then exit the sub
        AccountView.currentUser = ListBox1.SelectedItem
        AccountView.initializeForm()
        AccountView.Show()
        Me.Close()

    End Sub

    Public Sub refreshAccountList()
        ListBox1.Items.Clear()
        'Getting all the folders in the current directory
        For Each file As String In My.Computer.FileSystem.GetDirectories(My.Computer.FileSystem.CurrentDirectory)
            'Cleaning up foldername
            file = file.Replace(My.Computer.FileSystem.CurrentDirectory, "")
            If file.First = "/" Or file.First = "\" Then
                file = file.Substring(1)
            End If
            'Adding username to listview
            ListBox1.Items.Add(file)
        Next
        If ListBox1.Items.Count = 0 Then
            ListBox1.Items.Add("There are no accounts")
        End If
    End Sub

    Private Sub AnimationTimer_Tick(sender As Object, e As EventArgs) Handles AddAccountTimer.Tick
        'If the form is already expanded
        If expanded Then
            'Reduce the height of the form and move the button
            Me.Size = New Point(Me.Size.Width, Me.Size.Height - 3)
            addBtn.Location = New Point(addBtn.Location.X, addBtn.Location.Y - 3)
            'If the animation is done
            If addBtn.Location.Y <= 275 Then
                AddAccountTimer.Enabled = False
                'Changing the button label depending on the number of items selected
                If ListBox1.SelectedItems.Count > 0 Then
                    addBtn.Text = "Open"
                Else
                    addBtn.Text = "Add Account"
                End If

                expanded = False
            End If
        Else 'If its not expanded, expand it
            Me.Size = New Point(Me.Size.Width, Me.Size.Height + 3)
            addBtn.Location = New Point(addBtn.Location.X, addBtn.Location.Y + 3)

            If addBtn.Location.Y >= 305 Then
                AddAccountTimer.Enabled = False
                addBtn.Text = "Submit"
                expanded = True
            End If

        End If
    End Sub
    Public Function getAccountNames()
        refreshAccountList()
        Dim accounts As New ArrayList
        For Each item In ListBox1.Items
            accounts.Add(ListBox1.GetItemText(item))
        Next

        Return accounts
    End Function

    Private Sub addBtn_Click(sender As Object, e As EventArgs) Handles addBtn.Click
        'If its expanded then its creating a new account
        If expanded Then
            If analyzer.isValidInput(TextBox1.Text, "\", "/", ":", "?", """", ">", "<", "|") = False Then
                MsgBox("A account name cannot contain: \ / : * ? "" < > |")
                Exit Sub
            End If
            If analyzer.isValidInput(TextBox1.Text, "Enter Account Name") Then
                My.Computer.FileSystem.CreateDirectory(My.Computer.FileSystem.CurrentDirectory + "/" + analyzer.capitalize(TextBox1.Text))
            Else
                MsgBox("That is not a valid name.")
                Exit Sub
            End If

            AddAccountTimer.Enabled = True 'un-expand the form
            TextBox1.Text = "Enter Account Name"
            refreshAccountList()
        Else
            If addBtn.Text = "Open" Then
                If ListBox1.SelectedItem.ToString = "There are no accounts" Then
                    Exit Sub
                End If
                AccountView.currentUser = ListBox1.SelectedItem
                AccountView.initializeForm()
                AccountView.Show()
                Me.Hide()
                Exit Sub
            End If
            renameBtn.Visible = False
            deleteBtn.Visible = False
            AddAccountTimer.Enabled = True
        End If
    End Sub

    Private Sub TextBox1_GotFocus(sender As Object, e As EventArgs) Handles TextBox1.GotFocus
        If TextBox1.Text = "Enter Account Name" Then
            TextBox1.Text = ""
        End If

    End Sub

    Private Sub LoginScreen_MouseClick(sender As Object, e As MouseEventArgs) Handles MyBase.MouseClick
        ListBox1.SelectedItems.Clear()
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteToolStripMenuItem.Click
        If ListBox1.SelectedItems.Count = 0 Then
            MsgBox("Select an account to delete!")
        Else

            If MsgBox("Are you sure you want to delete " + ListBox1.SelectedItem + "'s account?", MsgBoxStyle.YesNoCancel) = MsgBoxResult.Yes Then
                My.Computer.FileSystem.DeleteDirectory(My.Computer.FileSystem.CurrentDirectory + "/" + ListBox1.SelectedItem, FileIO.DeleteDirectoryOption.DeleteAllContents)
                refreshAccountList()
                addBtn.Text = "Add Account"
                addBtn.FlatAppearance.BorderColor = Color.FromArgb(64, 64, 64)
                addBtn.FlatAppearance.MouseOverBackColor = Color.Gray
                If options Then
                    OptionsTimer.Enabled = True
                End If
            End If
        End If
    End Sub

    Private Sub RenameToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RenameToolStripMenuItem.Click
        If ListBox1.SelectedItems.Count = 0 Then
            MsgBox("Select an account to rename!")
        Else
            Dim newName As String = InputBox("Enter the new name for the account")
            If newName.Replace(" ", "") = "" Then Exit Sub
            newName = analyzer.capitalize(newName)
            If My.Computer.FileSystem.DirectoryExists(My.Computer.FileSystem.CurrentDirectory + "/" + newName) Then
                MsgBox("That account name already exists.")
                Exit Sub
            End If

            My.Computer.FileSystem.RenameDirectory(My.Computer.FileSystem.CurrentDirectory + "/" + ListBox1.SelectedItem, newName)
            refreshAccountList()

        End If

    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged
        If ListBox1.SelectedItems.Count = 0 Then
            addBtn.Text = "Add Account"
            addBtn.FlatAppearance.BorderColor = Color.FromArgb(64, 64, 64)
            addBtn.FlatAppearance.MouseOverBackColor = Color.Gray
            If options Then
                OptionsTimer.Enabled = True
            End If
        Else
            If expanded Then
                AddAccountTimer.Enabled = True
            End If


            If options = False Then
                renameBtn.Visible = True
                deleteBtn.Visible = True
                OptionsTimer.Enabled = True
            End If

            addBtn.Text = "Open"
            addBtn.FlatAppearance.BorderColor = Color.Green
            addBtn.FlatAppearance.MouseOverBackColor = Color.Lime
        End If
    End Sub

    Private Sub OptionsTimer_Tick_1(sender As Object, e As EventArgs) Handles OptionsTimer.Tick
        If AddAccountTimer.Enabled = True Then Exit Sub
        If options Then
            Me.Size = New Point(Me.Size.Width, Me.Size.Height - 3)
            If Me.Height <= 350 Then
                OptionsTimer.Enabled = False
                options = False
            End If
        Else
            Me.Size = New Point(Me.Size.Width, Me.Size.Height + 3)
            If Me.Height >= 380 Then
                OptionsTimer.Enabled = False

                options = True
            End If

        End If
    End Sub

    Private Sub renameBtn_Click(sender As Object, e As EventArgs) Handles renameBtn.Click
        RenameToolStripMenuItem.PerformClick()
    End Sub

    Private Sub deleteBtn_Click(sender As Object, e As EventArgs) Handles deleteBtn.Click
        DeleteToolStripMenuItem.PerformClick()
    End Sub
End Class