﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AccountView
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AccountView))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewClubToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CreateMasterListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CreateStudentReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshClubsListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImportClubToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BackToLoginScreenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CreateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AttendanceListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BusManifestToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EligibilityFormToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExcelDocumentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MailingListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PhotoGridToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListViewClubs = New System.Windows.Forms.ListView()
        Me.infoGroup = New System.Windows.Forms.GroupBox()
        Me.typeLabel = New System.Windows.Forms.Label()
        Me.typeSelector = New System.Windows.Forms.ComboBox()
        Me.infoCheckbox = New System.Windows.Forms.CheckBox()
        Me.totalMembers = New System.Windows.Forms.Label()
        Me.deleteBtn = New System.Windows.Forms.Button()
        Me.editSaveBtn = New System.Windows.Forms.Button()
        Me.addClassBtn = New System.Windows.Forms.Button()
        Me.removeStudentBtn = New System.Windows.Forms.Button()
        Me.addStudentBtn = New System.Windows.Forms.Button()
        Me.membersList = New System.Windows.Forms.ListBox()
        Me.clubNameLbl = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.clubNameTxt = New System.Windows.Forms.TextBox()
        Me.tempLabel = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.PhotoGrid = New System.Drawing.Printing.PrintDocument()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.addClubBtn = New System.Windows.Forms.Button()
        Me.deleteClubBtn = New System.Windows.Forms.Button()
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.NameList = New System.Drawing.Printing.PrintDocument()
        Me.parentEmail2Lbl = New System.Windows.Forms.Label()
        Me.parentEmail1Lbl = New System.Windows.Forms.Label()
        Me.bornLbl = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.stuEmailLbl = New System.Windows.Forms.Label()
        Me.guardTeleLbl = New System.Windows.Forms.Label()
        Me.stuTeleLbl = New System.Windows.Forms.Label()
        Me.numberLbl = New System.Windows.Forms.Label()
        Me.lastLbl = New System.Windows.Forms.Label()
        Me.firstLbl = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.infoPanelAnimationTimer = New System.Windows.Forms.Timer(Me.components)
        Me.CheckboxAnimationTimer = New System.Windows.Forms.Timer(Me.components)
        Me.BusManifest = New System.Drawing.Printing.PrintDocument()
        Me.commandTxtbox = New System.Windows.Forms.TextBox()
        Me.commandShakeTimer = New System.Windows.Forms.Timer(Me.components)
        Me.commandErrorLabel = New System.Windows.Forms.Label()
        Me.commandErrorTimer = New System.Windows.Forms.Timer(Me.components)
        Me.commandMoveTimer = New System.Windows.Forms.Timer(Me.components)
        Me.EligForm = New System.Drawing.Printing.PrintDocument()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.StudentReport = New System.Drawing.Printing.PrintDocument()
        Me.InitLabel = New System.Windows.Forms.Label()
        Me.studentPicture = New System.Windows.Forms.PictureBox()
        Me.MenuStrip1.SuspendLayout()
        Me.infoGroup.SuspendLayout()
        CType(Me.studentPicture, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.CreateToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(965, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewClubToolStripMenuItem, Me.CreateMasterListToolStripMenuItem, Me.CreateStudentReportToolStripMenuItem, Me.RefreshClubsListToolStripMenuItem, Me.ImportClubToolStripMenuItem, Me.BackToLoginScreenToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'NewClubToolStripMenuItem
        '
        Me.NewClubToolStripMenuItem.Name = "NewClubToolStripMenuItem"
        Me.NewClubToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.NewClubToolStripMenuItem.Text = "New Club"
        '
        'CreateMasterListToolStripMenuItem
        '
        Me.CreateMasterListToolStripMenuItem.Name = "CreateMasterListToolStripMenuItem"
        Me.CreateMasterListToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.CreateMasterListToolStripMenuItem.Text = "Create Master List"
        '
        'CreateStudentReportToolStripMenuItem
        '
        Me.CreateStudentReportToolStripMenuItem.Name = "CreateStudentReportToolStripMenuItem"
        Me.CreateStudentReportToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.CreateStudentReportToolStripMenuItem.Text = "Create Student Report"
        '
        'RefreshClubsListToolStripMenuItem
        '
        Me.RefreshClubsListToolStripMenuItem.Name = "RefreshClubsListToolStripMenuItem"
        Me.RefreshClubsListToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.RefreshClubsListToolStripMenuItem.Text = "Refresh Clubs List"
        '
        'ImportClubToolStripMenuItem
        '
        Me.ImportClubToolStripMenuItem.Name = "ImportClubToolStripMenuItem"
        Me.ImportClubToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.ImportClubToolStripMenuItem.Text = "Import Club"
        '
        'BackToLoginScreenToolStripMenuItem
        '
        Me.BackToLoginScreenToolStripMenuItem.Name = "BackToLoginScreenToolStripMenuItem"
        Me.BackToLoginScreenToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.BackToLoginScreenToolStripMenuItem.Text = "Go Back to Login Screen"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'CreateToolStripMenuItem
        '
        Me.CreateToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AttendanceListToolStripMenuItem, Me.EligibilityFormToolStripMenuItem, Me.ExcelDocumentToolStripMenuItem, Me.MailingListToolStripMenuItem, Me.PhotoGridToolStripMenuItem, Me.BusManifestToolStripMenuItem})
        Me.CreateToolStripMenuItem.Name = "CreateToolStripMenuItem"
        Me.CreateToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
        Me.CreateToolStripMenuItem.Text = "Create"
        Me.CreateToolStripMenuItem.Visible = False
        '
        'AttendanceListToolStripMenuItem
        '
        Me.AttendanceListToolStripMenuItem.Name = "AttendanceListToolStripMenuItem"
        Me.AttendanceListToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.AttendanceListToolStripMenuItem.Text = "Attendance List"
        '
        'BusManifestToolStripMenuItem
        '
        Me.BusManifestToolStripMenuItem.Name = "BusManifestToolStripMenuItem"
        Me.BusManifestToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.BusManifestToolStripMenuItem.Text = "Student Manifest"
        '
        'EligibilityFormToolStripMenuItem
        '
        Me.EligibilityFormToolStripMenuItem.Name = "EligibilityFormToolStripMenuItem"
        Me.EligibilityFormToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.EligibilityFormToolStripMenuItem.Text = "Eligibility Form"
        '
        'ExcelDocumentToolStripMenuItem
        '
        Me.ExcelDocumentToolStripMenuItem.Name = "ExcelDocumentToolStripMenuItem"
        Me.ExcelDocumentToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.ExcelDocumentToolStripMenuItem.Text = "Excel Document"
        '
        'MailingListToolStripMenuItem
        '
        Me.MailingListToolStripMenuItem.Name = "MailingListToolStripMenuItem"
        Me.MailingListToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.MailingListToolStripMenuItem.Text = "Mailing List"
        '
        'PhotoGridToolStripMenuItem
        '
        Me.PhotoGridToolStripMenuItem.Name = "PhotoGridToolStripMenuItem"
        Me.PhotoGridToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.PhotoGridToolStripMenuItem.Text = "Photo Grid"
        '
        'ListViewClubs
        '
        Me.ListViewClubs.FullRowSelect = True
        Me.ListViewClubs.GridLines = True
        Me.ListViewClubs.Location = New System.Drawing.Point(12, 49)
        Me.ListViewClubs.Name = "ListViewClubs"
        Me.ListViewClubs.Size = New System.Drawing.Size(292, 373)
        Me.ListViewClubs.TabIndex = 1
        Me.ListViewClubs.UseCompatibleStateImageBehavior = False
        Me.ListViewClubs.View = System.Windows.Forms.View.Details
        '
        'infoGroup
        '
        Me.infoGroup.Controls.Add(Me.typeLabel)
        Me.infoGroup.Controls.Add(Me.typeSelector)
        Me.infoGroup.Controls.Add(Me.infoCheckbox)
        Me.infoGroup.Controls.Add(Me.totalMembers)
        Me.infoGroup.Controls.Add(Me.deleteBtn)
        Me.infoGroup.Controls.Add(Me.editSaveBtn)
        Me.infoGroup.Controls.Add(Me.addClassBtn)
        Me.infoGroup.Controls.Add(Me.removeStudentBtn)
        Me.infoGroup.Controls.Add(Me.addStudentBtn)
        Me.infoGroup.Controls.Add(Me.membersList)
        Me.infoGroup.Controls.Add(Me.clubNameLbl)
        Me.infoGroup.Controls.Add(Me.Label1)
        Me.infoGroup.Controls.Add(Me.clubNameTxt)
        Me.infoGroup.Controls.Add(Me.tempLabel)
        Me.infoGroup.Location = New System.Drawing.Point(366, 49)
        Me.infoGroup.Name = "infoGroup"
        Me.infoGroup.Size = New System.Drawing.Size(328, 415)
        Me.infoGroup.TabIndex = 2
        Me.infoGroup.TabStop = False
        Me.infoGroup.Text = "Information"
        '
        'typeLabel
        '
        Me.typeLabel.AutoSize = True
        Me.typeLabel.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
        Me.typeLabel.Location = New System.Drawing.Point(94, 26)
        Me.typeLabel.Name = "typeLabel"
        Me.typeLabel.Size = New System.Drawing.Size(62, 19)
        Me.typeLabel.TabIndex = 13
        Me.typeLabel.Text = "Label11"
        '
        'typeSelector
        '
        Me.typeSelector.FormattingEnabled = True
        Me.typeSelector.Location = New System.Drawing.Point(202, 24)
        Me.typeSelector.Name = "typeSelector"
        Me.typeSelector.Size = New System.Drawing.Size(121, 21)
        Me.typeSelector.TabIndex = 12
        Me.typeSelector.Text = "Select a type"
        '
        'infoCheckbox
        '
        Me.infoCheckbox.AutoSize = True
        Me.infoCheckbox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.infoCheckbox.Location = New System.Drawing.Point(92, 341)
        Me.infoCheckbox.Name = "infoCheckbox"
        Me.infoCheckbox.Size = New System.Drawing.Size(162, 20)
        Me.infoCheckbox.TabIndex = 11
        Me.infoCheckbox.Text = "Show more information"
        Me.infoCheckbox.UseVisualStyleBackColor = True
        '
        'totalMembers
        '
        Me.totalMembers.AutoSize = True
        Me.totalMembers.Location = New System.Drawing.Point(30, 74)
        Me.totalMembers.Name = "totalMembers"
        Me.totalMembers.Size = New System.Drawing.Size(0, 13)
        Me.totalMembers.TabIndex = 3
        '
        'deleteBtn
        '
        Me.deleteBtn.FlatAppearance.BorderColor = System.Drawing.Color.Red
        Me.deleteBtn.FlatAppearance.BorderSize = 2
        Me.deleteBtn.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark
        Me.deleteBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.deleteBtn.ForeColor = System.Drawing.Color.Red
        Me.deleteBtn.Location = New System.Drawing.Point(12, 376)
        Me.deleteBtn.Name = "deleteBtn"
        Me.deleteBtn.Size = New System.Drawing.Size(75, 23)
        Me.deleteBtn.TabIndex = 10
        Me.deleteBtn.Text = "Delete"
        Me.deleteBtn.UseVisualStyleBackColor = True
        '
        'editSaveBtn
        '
        Me.editSaveBtn.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark
        Me.editSaveBtn.FlatAppearance.BorderSize = 2
        Me.editSaveBtn.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark
        Me.editSaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.editSaveBtn.Location = New System.Drawing.Point(237, 376)
        Me.editSaveBtn.Name = "editSaveBtn"
        Me.editSaveBtn.Size = New System.Drawing.Size(75, 23)
        Me.editSaveBtn.TabIndex = 9
        Me.editSaveBtn.Text = "Edit"
        Me.editSaveBtn.UseVisualStyleBackColor = True
        '
        'addClassBtn
        '
        Me.addClassBtn.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.addClassBtn.FlatAppearance.BorderSize = 2
        Me.addClassBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green
        Me.addClassBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.addClassBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.addClassBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.addClassBtn.Location = New System.Drawing.Point(260, 169)
        Me.addClassBtn.Margin = New System.Windows.Forms.Padding(0)
        Me.addClassBtn.Name = "addClassBtn"
        Me.addClassBtn.Size = New System.Drawing.Size(63, 45)
        Me.addClassBtn.TabIndex = 7
        Me.addClassBtn.Text = "Add Class"
        Me.addClassBtn.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.addClassBtn.UseVisualStyleBackColor = True
        '
        'removeStudentBtn
        '
        Me.removeStudentBtn.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.removeStudentBtn.FlatAppearance.BorderSize = 2
        Me.removeStudentBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon
        Me.removeStudentBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.removeStudentBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.removeStudentBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!)
        Me.removeStudentBtn.Location = New System.Drawing.Point(269, 115)
        Me.removeStudentBtn.Margin = New System.Windows.Forms.Padding(0)
        Me.removeStudentBtn.Name = "removeStudentBtn"
        Me.removeStudentBtn.Size = New System.Drawing.Size(44, 45)
        Me.removeStudentBtn.TabIndex = 7
        Me.removeStudentBtn.Text = "-"
        Me.removeStudentBtn.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.removeStudentBtn.UseVisualStyleBackColor = True
        '
        'addStudentBtn
        '
        Me.addStudentBtn.AutoSize = True
        Me.addStudentBtn.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.addStudentBtn.FlatAppearance.BorderSize = 2
        Me.addStudentBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green
        Me.addStudentBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.addStudentBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.addStudentBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!)
        Me.addStudentBtn.Location = New System.Drawing.Point(269, 61)
        Me.addStudentBtn.Margin = New System.Windows.Forms.Padding(0)
        Me.addStudentBtn.Name = "addStudentBtn"
        Me.addStudentBtn.Size = New System.Drawing.Size(44, 45)
        Me.addStudentBtn.TabIndex = 7
        Me.addStudentBtn.Text = "+"
        Me.addStudentBtn.UseVisualStyleBackColor = True
        '
        'membersList
        '
        Me.membersList.FormattingEnabled = True
        Me.membersList.HorizontalScrollbar = True
        Me.membersList.IntegralHeight = False
        Me.membersList.Location = New System.Drawing.Point(88, 61)
        Me.membersList.Name = "membersList"
        Me.membersList.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.membersList.Size = New System.Drawing.Size(171, 303)
        Me.membersList.TabIndex = 3
        '
        'clubNameLbl
        '
        Me.clubNameLbl.AutoSize = True
        Me.clubNameLbl.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.clubNameLbl.Location = New System.Drawing.Point(29, 26)
        Me.clubNameLbl.Name = "clubNameLbl"
        Me.clubNameLbl.Size = New System.Drawing.Size(58, 19)
        Me.clubNameLbl.TabIndex = 3
        Me.clubNameLbl.Text = "NAME"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(29, 61)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Members: "
        '
        'clubNameTxt
        '
        Me.clubNameTxt.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.clubNameTxt.Location = New System.Drawing.Point(32, 23)
        Me.clubNameTxt.Name = "clubNameTxt"
        Me.clubNameTxt.Size = New System.Drawing.Size(165, 26)
        Me.clubNameTxt.TabIndex = 5
        Me.clubNameTxt.Text = "Enter Club Name"
        '
        'tempLabel
        '
        Me.tempLabel.AutoSize = True
        Me.tempLabel.Location = New System.Drawing.Point(88, 186)
        Me.tempLabel.Name = "tempLabel"
        Me.tempLabel.Size = New System.Drawing.Size(149, 13)
        Me.tempLabel.TabIndex = 8
        Me.tempLabel.Text = "Click on a club for information."
        '
        'PhotoGrid
        '
        Me.PhotoGrid.OriginAtMargins = True
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.UseAntiAlias = True
        Me.PrintPreviewDialog1.Visible = False
        '
        'addClubBtn
        '
        Me.addClubBtn.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.addClubBtn.FlatAppearance.BorderSize = 2
        Me.addClubBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green
        Me.addClubBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.addClubBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.addClubBtn.Location = New System.Drawing.Point(205, 429)
        Me.addClubBtn.Name = "addClubBtn"
        Me.addClubBtn.Size = New System.Drawing.Size(99, 36)
        Me.addClubBtn.TabIndex = 3
        Me.addClubBtn.Text = "Add Club"
        Me.addClubBtn.UseVisualStyleBackColor = True
        '
        'deleteClubBtn
        '
        Me.deleteClubBtn.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.deleteClubBtn.FlatAppearance.BorderSize = 2
        Me.deleteClubBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red
        Me.deleteClubBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.deleteClubBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.deleteClubBtn.Location = New System.Drawing.Point(12, 429)
        Me.deleteClubBtn.Name = "deleteClubBtn"
        Me.deleteClubBtn.Size = New System.Drawing.Size(99, 36)
        Me.deleteClubBtn.TabIndex = 3
        Me.deleteClubBtn.Text = "Delete Club"
        Me.deleteClubBtn.UseVisualStyleBackColor = True
        '
        'PrintDialog1
        '
        Me.PrintDialog1.AllowCurrentPage = True
        Me.PrintDialog1.AllowSomePages = True
        Me.PrintDialog1.Document = Me.PhotoGrid
        Me.PrintDialog1.UseEXDialog = True
        '
        'NameList
        '
        '
        'parentEmail2Lbl
        '
        Me.parentEmail2Lbl.AutoSize = True
        Me.parentEmail2Lbl.Location = New System.Drawing.Point(830, 412)
        Me.parentEmail2Lbl.Name = "parentEmail2Lbl"
        Me.parentEmail2Lbl.Size = New System.Drawing.Size(30, 13)
        Me.parentEmail2Lbl.TabIndex = 39
        Me.parentEmail2Lbl.Text = "temp"
        '
        'parentEmail1Lbl
        '
        Me.parentEmail1Lbl.AutoSize = True
        Me.parentEmail1Lbl.Location = New System.Drawing.Point(830, 397)
        Me.parentEmail1Lbl.Name = "parentEmail1Lbl"
        Me.parentEmail1Lbl.Size = New System.Drawing.Size(30, 13)
        Me.parentEmail1Lbl.TabIndex = 38
        Me.parentEmail1Lbl.Text = "temp"
        '
        'bornLbl
        '
        Me.bornLbl.AutoSize = True
        Me.bornLbl.Location = New System.Drawing.Point(757, 251)
        Me.bornLbl.Name = "bornLbl"
        Me.bornLbl.Size = New System.Drawing.Size(30, 13)
        Me.bornLbl.TabIndex = 37
        Me.bornLbl.Text = "temp"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label9.Location = New System.Drawing.Point(721, 247)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(37, 17)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "Age:"
        '
        'stuEmailLbl
        '
        Me.stuEmailLbl.AutoSize = True
        Me.stuEmailLbl.Location = New System.Drawing.Point(819, 369)
        Me.stuEmailLbl.Name = "stuEmailLbl"
        Me.stuEmailLbl.Size = New System.Drawing.Size(30, 13)
        Me.stuEmailLbl.TabIndex = 35
        Me.stuEmailLbl.Text = "temp"
        '
        'guardTeleLbl
        '
        Me.guardTeleLbl.AutoSize = True
        Me.guardTeleLbl.Location = New System.Drawing.Point(789, 340)
        Me.guardTeleLbl.Name = "guardTeleLbl"
        Me.guardTeleLbl.Size = New System.Drawing.Size(30, 13)
        Me.guardTeleLbl.TabIndex = 34
        Me.guardTeleLbl.Text = "temp"
        '
        'stuTeleLbl
        '
        Me.stuTeleLbl.AutoSize = True
        Me.stuTeleLbl.Location = New System.Drawing.Point(788, 310)
        Me.stuTeleLbl.Name = "stuTeleLbl"
        Me.stuTeleLbl.Size = New System.Drawing.Size(30, 13)
        Me.stuTeleLbl.TabIndex = 33
        Me.stuTeleLbl.Text = "temp"
        '
        'numberLbl
        '
        Me.numberLbl.AutoSize = True
        Me.numberLbl.Location = New System.Drawing.Point(833, 280)
        Me.numberLbl.Name = "numberLbl"
        Me.numberLbl.Size = New System.Drawing.Size(30, 13)
        Me.numberLbl.TabIndex = 32
        Me.numberLbl.Text = "temp"
        '
        'lastLbl
        '
        Me.lastLbl.AutoSize = True
        Me.lastLbl.Location = New System.Drawing.Point(757, 225)
        Me.lastLbl.Name = "lastLbl"
        Me.lastLbl.Size = New System.Drawing.Size(30, 13)
        Me.lastLbl.TabIndex = 30
        Me.lastLbl.Text = "temp"
        '
        'firstLbl
        '
        Me.firstLbl.AutoSize = True
        Me.firstLbl.Location = New System.Drawing.Point(759, 196)
        Me.firstLbl.Name = "firstLbl"
        Me.firstLbl.Size = New System.Drawing.Size(30, 13)
        Me.firstLbl.TabIndex = 29
        Me.firstLbl.Text = "temp"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(721, 222)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 17)
        Me.Label3.TabIndex = 27
        Me.Label3.Text = "Last:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(721, 192)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 17)
        Me.Label4.TabIndex = 26
        Me.Label4.Text = "First: "
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label10.Location = New System.Drawing.Point(721, 395)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(109, 17)
        Me.Label10.TabIndex = 25
        Me.Label10.Text = "Parent Email(s):"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label8.Location = New System.Drawing.Point(721, 367)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(100, 17)
        Me.Label8.TabIndex = 24
        Me.Label8.Text = "Student Email:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label7.Location = New System.Drawing.Point(721, 337)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(69, 17)
        Me.Label7.TabIndex = 23
        Me.Label7.Text = "Phone #2:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label6.Location = New System.Drawing.Point(721, 307)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(69, 17)
        Me.Label6.TabIndex = 22
        Me.Label6.Text = "Phone #1:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(721, 277)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(114, 17)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Student Number:"
        '
        'infoPanelAnimationTimer
        '
        Me.infoPanelAnimationTimer.Interval = 10
        '
        'CheckboxAnimationTimer
        '
        Me.CheckboxAnimationTimer.Interval = 10
        '
        'BusManifest
        '
        '
        'commandTxtbox
        '
        Me.commandTxtbox.AllowDrop = True
        Me.commandTxtbox.AutoCompleteCustomSource.AddRange(New String() {"Help: Add <Club Name>", "Help: Delete <Club Name>", "Help: Edit <Club Name>", "Help: Rename <Old Club Name> , <New Club Name>", "Help: Print Attendance List <Club Name>", "Help: Print Bus Manifest <Club Name>", "Help: Print Photo Grid <Club Name>", "Help: Preview Attendance List <Club Name>", "Help: Preview Bus Manifest <Club Name>", "Help: Preview Photo Grid <Club Name>"})
        Me.commandTxtbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.commandTxtbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.commandTxtbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.commandTxtbox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.commandTxtbox.ForeColor = System.Drawing.Color.Black
        Me.commandTxtbox.Location = New System.Drawing.Point(214, 100)
        Me.commandTxtbox.Name = "commandTxtbox"
        Me.commandTxtbox.Size = New System.Drawing.Size(300, 22)
        Me.commandTxtbox.TabIndex = 42
        Me.commandTxtbox.WordWrap = False
        '
        'commandShakeTimer
        '
        Me.commandShakeTimer.Interval = 50
        '
        'commandErrorLabel
        '
        Me.commandErrorLabel.AutoSize = True
        Me.commandErrorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.commandErrorLabel.ForeColor = System.Drawing.Color.Red
        Me.commandErrorLabel.Location = New System.Drawing.Point(211, 125)
        Me.commandErrorLabel.Name = "commandErrorLabel"
        Me.commandErrorLabel.Size = New System.Drawing.Size(56, 16)
        Me.commandErrorLabel.TabIndex = 43
        Me.commandErrorLabel.Text = "Label11"
        Me.commandErrorLabel.Visible = False
        '
        'commandErrorTimer
        '
        Me.commandErrorTimer.Interval = 4000
        '
        'commandMoveTimer
        '
        Me.commandMoveTimer.Interval = 10
        '
        'EligForm
        '
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Filter = "Text files|*.txt|All files|*.*"
        Me.OpenFileDialog1.Multiselect = True
        '
        'StudentReport
        '
        Me.StudentReport.OriginAtMargins = True
        '
        'InitLabel
        '
        Me.InitLabel.AutoSize = True
        Me.InitLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InitLabel.Location = New System.Drawing.Point(22, 251)
        Me.InitLabel.Name = "InitLabel"
        Me.InitLabel.Size = New System.Drawing.Size(383, 73)
        Me.InitLabel.TabIndex = 14
        Me.InitLabel.Text = "Initializing..."
        Me.InitLabel.Visible = False
        '
        'studentPicture
        '
        Me.studentPicture.Image = CType(resources.GetObject("studentPicture.Image"), System.Drawing.Image)
        Me.studentPicture.Location = New System.Drawing.Point(724, 38)
        Me.studentPicture.Name = "studentPicture"
        Me.studentPicture.Size = New System.Drawing.Size(110, 140)
        Me.studentPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.studentPicture.TabIndex = 20
        Me.studentPicture.TabStop = False
        '
        'AccountView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(965, 501)
        Me.Controls.Add(Me.commandErrorLabel)
        Me.Controls.Add(Me.commandTxtbox)
        Me.Controls.Add(Me.parentEmail2Lbl)
        Me.Controls.Add(Me.parentEmail1Lbl)
        Me.Controls.Add(Me.bornLbl)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.stuEmailLbl)
        Me.Controls.Add(Me.guardTeleLbl)
        Me.Controls.Add(Me.stuTeleLbl)
        Me.Controls.Add(Me.numberLbl)
        Me.Controls.Add(Me.lastLbl)
        Me.Controls.Add(Me.firstLbl)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.InitLabel)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.studentPicture)
        Me.Controls.Add(Me.deleteClubBtn)
        Me.Controls.Add(Me.addClubBtn)
        Me.Controls.Add(Me.ListViewClubs)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.infoGroup)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "AccountView"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.infoGroup.ResumeLayout(False)
        Me.infoGroup.PerformLayout()
        CType(Me.studentPicture, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewClubToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListViewClubs As System.Windows.Forms.ListView
    Friend WithEvents infoGroup As System.Windows.Forms.GroupBox
    Friend WithEvents membersList As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents clubNameLbl As System.Windows.Forms.Label
    Friend WithEvents removeStudentBtn As System.Windows.Forms.Button
    Friend WithEvents addStudentBtn As System.Windows.Forms.Button
    Friend WithEvents clubNameTxt As System.Windows.Forms.TextBox
    Friend WithEvents tempLabel As System.Windows.Forms.Label
    Friend WithEvents editSaveBtn As System.Windows.Forms.Button
    Friend WithEvents RefreshClubsListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents deleteBtn As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents PhotoGrid As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintPreviewDialog1 As System.Windows.Forms.PrintPreviewDialog
    Friend WithEvents totalMembers As System.Windows.Forms.Label
    Friend WithEvents addClubBtn As System.Windows.Forms.Button
    Friend WithEvents deleteClubBtn As System.Windows.Forms.Button
    Friend WithEvents PrintDialog1 As System.Windows.Forms.PrintDialog
    Friend WithEvents NameList As System.Drawing.Printing.PrintDocument
    Friend WithEvents BackToLoginScreenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents parentEmail2Lbl As System.Windows.Forms.Label
    Friend WithEvents parentEmail1Lbl As System.Windows.Forms.Label
    Friend WithEvents bornLbl As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents stuEmailLbl As System.Windows.Forms.Label
    Friend WithEvents guardTeleLbl As System.Windows.Forms.Label
    Friend WithEvents stuTeleLbl As System.Windows.Forms.Label
    Friend WithEvents numberLbl As System.Windows.Forms.Label
    Friend WithEvents lastLbl As System.Windows.Forms.Label
    Friend WithEvents firstLbl As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents studentPicture As System.Windows.Forms.PictureBox
    Friend WithEvents infoPanelAnimationTimer As System.Windows.Forms.Timer
    Friend WithEvents infoCheckbox As System.Windows.Forms.CheckBox
    Friend WithEvents CheckboxAnimationTimer As System.Windows.Forms.Timer
    Friend WithEvents BusManifest As System.Drawing.Printing.PrintDocument
    Friend WithEvents commandTxtbox As System.Windows.Forms.TextBox
    Friend WithEvents commandShakeTimer As System.Windows.Forms.Timer
    Friend WithEvents commandErrorLabel As System.Windows.Forms.Label
    Friend WithEvents commandErrorTimer As System.Windows.Forms.Timer
    Friend WithEvents commandMoveTimer As System.Windows.Forms.Timer
    Friend WithEvents CreateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AttendanceListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BusManifestToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExcelDocumentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PhotoGridToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EligForm As System.Drawing.Printing.PrintDocument
    Friend WithEvents MailingListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EligibilityFormToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CreateMasterListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImportClubToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents addClassBtn As System.Windows.Forms.Button
    Friend WithEvents typeSelector As System.Windows.Forms.ComboBox
    Friend WithEvents typeLabel As System.Windows.Forms.Label
    Friend WithEvents CreateStudentReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StudentReport As System.Drawing.Printing.PrintDocument
    Friend WithEvents InitLabel As System.Windows.Forms.Label
End Class
