﻿Imports System.IO

Public Class FileAnalyzer
    Public catagories As New ArrayList
    Public data As New ArrayList
    Public numberDictionary As New Dictionary(Of String, ArrayList)

    Public Sub readFile(ByVal filepath As String)
        data.Clear()
        numberDictionary.Clear()

        'Reading the input
        Dim reader As New StreamReader(filepath)
        catagories = toArrayList(reader.ReadLine.Split(","))
    
        While reader.EndOfStream = False
            data.Add(handleQuotes(toArrayList(reader.ReadLine.Split(","))))
        End While

        reader.Close()

        Dim studentNumber As String
        Dim dicStudentInfo As New ArrayList
        For Each studentInfo As ArrayList In data
            studentNumber = studentInfo.Item(3) ' 3 is the index of the student number in the array
            dicStudentInfo = studentInfo.Clone
            dicStudentInfo.RemoveAt(3)
            numberDictionary.Add(studentNumber, dicStudentInfo)
        Next

    End Sub

    Public Function getNameFromNumber(ByVal studentNumber As String) As String
        If numberDictionary.Keys.Contains(studentNumber) = False Then Return "NTFOUND!"
        Return numberDictionary(studentNumber).Item(1) + " " + numberDictionary(studentNumber).Item(0)
    End Function
    Public Function getNumberFromAttendanceName(ByVal studentNumbersList, ByVal studentName) As String

        For Each studentNumber As String In studentNumbersList
            If studentName = getAttendanceNameFromNumber(studentNumber) Then
                Return studentNumber
            End If
        Next
        Return vbNull
    End Function

    Public Function getAttendanceNameFromNumber(ByVal studentNumber As String) As String
        Dim name As String
        Try
            name = numberDictionary(studentNumber).Item(0)
        Catch ex As Exception
            Return "NTFOUND"
        End Try

        Return numberDictionary(studentNumber).Item(0) + ", " + numberDictionary(studentNumber).Item(1)
    End Function


    Private Function toArrayList(ByVal SystemArray() As Object)
        Dim i As Object
        Dim arrayList As New ArrayList
        For Each i In SystemArray
            arrayList.Add(i)
        Next

        Return arrayList
    End Function

    Private Function handleQuotes(ByRef array As ArrayList)
        Dim startIndex As Integer = -1
        Dim endIndex As Integer = -1

        For index = 0 To array.Count - 1
            If array.Item(index).ToString.Contains(ControlChars.Quote) Then
                If startIndex = -1 Then
                    startIndex = index
                Else
                    endIndex = index
                    Exit For
                End If
            End If
        Next

        If startIndex = -1 Or endIndex = -1 Then
            Return array
        End If

        For ind = startIndex + 1 To endIndex
            array(startIndex) = array(startIndex).ToString.Replace(ControlChars.Quote, "") + "," + array(startIndex + 1).ToString.Replace(ControlChars.Quote, "")
            array.RemoveAt(startIndex + 1)
        Next
        handleQuotes(array)
        Return array
    End Function

    Public Function getTableviewData()
        Dim returnValue As New ArrayList
        Dim tempArray As New ArrayList

        For Each student As ArrayList In data
            tempArray.Clear()
            For i = 0 To 3
                tempArray.Add(student.Item(i))
            Next
            returnValue.Add(tempArray.Clone)
        Next

        Return returnValue
    End Function

    Public Function capitalize(rawString As String) As String
        Dim newString As String = ""
        Dim stringArray As New ArrayList

        If rawString.Contains(" ") Then
            stringArray = toArrayList(rawString.Split(" "))
        Else
            stringArray.Add(rawString)
        End If


        For Each word As String In stringArray
            If word.Length > 0 Then
                newString = newString + word.Substring(0, 1).ToUpper + word.Substring(1) + " "
            End If

        Next
        newString = newString.Remove(newString.Length - 1)


        Return newString
    End Function

    Public Function isValidInput(ByVal input As String, ByVal ParamArray invalidKeywords() As String) As Boolean
        Dim valid As Boolean = True

        If input.Replace(" ", "") = "" Then
            valid = False
        Else

            For Each badKeyword As String In invalidKeywords
                If input.ToLower.Contains(badKeyword.ToLower) Then
                    valid = False
                    Exit For
                End If
            Next

        End If

        Return valid

    End Function

    Public Function studentMatchesSearchString(ByVal student As ArrayList, ByVal searchString As String) As Boolean
      

        Dim matches As New ArrayList
        'Splits student name on spaces and puts it in an array
        Dim partsToSearch As New ArrayList From {student(3).ToString}
        partsToSearch.AddRange(student(0).ToString.Split(" "))
        partsToSearch.AddRange(student(1).ToString.Split(" "))

        'Adds True or False to the matches array depending on if the search term matches the word
        For Each word As String In partsToSearch
            matches.Add(word.ToLower.Substring(0, IIf(searchString.Length > word.Length, word.Length, searchString.Length)).Contains(searchString))
        Next

        'If there is at least one match then return true
        Return matches.Contains(True)
    End Function
End Class
