﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CreateSelection
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreateSelection))
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.cancelBtn = New System.Windows.Forms.Button()
        Me.printBtn = New System.Windows.Forms.Button()
        Me.selectNoneBtn = New System.Windows.Forms.Button()
        Me.selectAllBtn = New System.Windows.Forms.Button()
        Me.searchTimer = New System.Windows.Forms.Timer(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.previewBtn = New System.Windows.Forms.Button()
        Me.previewBtnAnimation = New System.Windows.Forms.Timer(Me.components)
        Me.previewBtnCooldownTimer = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'ListView1
        '
        Me.ListView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListView1.CheckBoxes = True
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.ListView1.GridLines = True
        Me.ListView1.Location = New System.Drawing.Point(14, 44)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(238, 336)
        Me.ListView1.TabIndex = 4
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Student Name"
        Me.ColumnHeader1.Width = 236
        '
        'TextBox1
        '
        Me.TextBox1.ForeColor = System.Drawing.Color.Gray
        Me.TextBox1.Location = New System.Drawing.Point(14, 14)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(238, 20)
        Me.TextBox1.TabIndex = 9
        Me.TextBox1.Text = "Enter search term here"
        '
        'cancelBtn
        '
        Me.cancelBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cancelBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cancelBtn.FlatAppearance.BorderSize = 2
        Me.cancelBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon
        Me.cancelBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cancelBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cancelBtn.Font = New System.Drawing.Font("Times New Roman", 12.0!)
        Me.cancelBtn.Location = New System.Drawing.Point(14, 422)
        Me.cancelBtn.Name = "cancelBtn"
        Me.cancelBtn.Size = New System.Drawing.Size(110, 28)
        Me.cancelBtn.TabIndex = 5
        Me.cancelBtn.Text = "Cancel"
        Me.cancelBtn.UseVisualStyleBackColor = True
        '
        'printBtn
        '
        Me.printBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.printBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.printBtn.FlatAppearance.BorderSize = 2
        Me.printBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green
        Me.printBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.printBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.printBtn.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.printBtn.Location = New System.Drawing.Point(136, 422)
        Me.printBtn.Name = "printBtn"
        Me.printBtn.Size = New System.Drawing.Size(110, 28)
        Me.printBtn.TabIndex = 6
        Me.printBtn.Text = "Print"
        Me.printBtn.UseVisualStyleBackColor = True
        '
        'selectNoneBtn
        '
        Me.selectNoneBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.selectNoneBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.selectNoneBtn.FlatAppearance.BorderSize = 2
        Me.selectNoneBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.selectNoneBtn.Font = New System.Drawing.Font("Times New Roman", 12.0!)
        Me.selectNoneBtn.Location = New System.Drawing.Point(14, 386)
        Me.selectNoneBtn.Name = "selectNoneBtn"
        Me.selectNoneBtn.Size = New System.Drawing.Size(110, 28)
        Me.selectNoneBtn.TabIndex = 7
        Me.selectNoneBtn.Text = "Select None"
        Me.selectNoneBtn.UseVisualStyleBackColor = True
        '
        'selectAllBtn
        '
        Me.selectAllBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.selectAllBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.selectAllBtn.FlatAppearance.BorderSize = 2
        Me.selectAllBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.selectAllBtn.Font = New System.Drawing.Font("Times New Roman", 12.0!)
        Me.selectAllBtn.Location = New System.Drawing.Point(136, 386)
        Me.selectAllBtn.Name = "selectAllBtn"
        Me.selectAllBtn.Size = New System.Drawing.Size(110, 28)
        Me.selectAllBtn.TabIndex = 8
        Me.selectAllBtn.Text = "Select All"
        Me.selectAllBtn.UseVisualStyleBackColor = True
        '
        'searchTimer
        '
        Me.searchTimer.Interval = 500
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.Label1.Location = New System.Drawing.Point(71, 206)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(134, 31)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Loading..."
        Me.Label1.Visible = False
        '
        'previewBtn
        '
        Me.previewBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.previewBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.previewBtn.FlatAppearance.BorderSize = 2
        Me.previewBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green
        Me.previewBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.previewBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.previewBtn.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.previewBtn.Location = New System.Drawing.Point(136, 422)
        Me.previewBtn.Name = "previewBtn"
        Me.previewBtn.Size = New System.Drawing.Size(110, 28)
        Me.previewBtn.TabIndex = 11
        Me.previewBtn.Text = "Preview"
        Me.previewBtn.UseVisualStyleBackColor = True
        '
        'previewBtnAnimation
        '
        Me.previewBtnAnimation.Enabled = True
        Me.previewBtnAnimation.Interval = 10
        '
        'previewBtnCooldownTimer
        '
        Me.previewBtnCooldownTimer.Interval = 500
        '
        'CreateSelection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(267, 462)
        Me.Controls.Add(Me.ListView1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.cancelBtn)
        Me.Controls.Add(Me.printBtn)
        Me.Controls.Add(Me.selectNoneBtn)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.previewBtn)
        Me.Controls.Add(Me.selectAllBtn)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "CreateSelection"
        Me.Text = "Create Selection"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents cancelBtn As System.Windows.Forms.Button
    Friend WithEvents selectNoneBtn As System.Windows.Forms.Button
    Friend WithEvents selectAllBtn As System.Windows.Forms.Button
    Friend WithEvents searchTimer As System.Windows.Forms.Timer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents previewBtn As System.Windows.Forms.Button
    Friend WithEvents previewBtnAnimation As System.Windows.Forms.Timer
    Friend WithEvents previewBtnCooldownTimer As System.Windows.Forms.Timer
    Private WithEvents printBtn As System.Windows.Forms.Button
End Class
