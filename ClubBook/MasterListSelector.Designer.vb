﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MasterListSelector
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.typesList = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cancelBtn = New System.Windows.Forms.Button()
        Me.createBtn = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'typesList
        '
        Me.typesList.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.typesList.CheckBoxes = True
        Me.typesList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.typesList.GridLines = True
        Me.typesList.Location = New System.Drawing.Point(21, 12)
        Me.typesList.Name = "typesList"
        Me.typesList.Scrollable = False
        Me.typesList.Size = New System.Drawing.Size(141, 170)
        Me.typesList.TabIndex = 5
        Me.typesList.UseCompatibleStateImageBehavior = False
        Me.typesList.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Types"
        Me.ColumnHeader1.Width = 136
        '
        'cancelBtn
        '
        Me.cancelBtn.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cancelBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cancelBtn.FlatAppearance.BorderSize = 2
        Me.cancelBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon
        Me.cancelBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cancelBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cancelBtn.Font = New System.Drawing.Font("Times New Roman", 12.0!)
        Me.cancelBtn.Location = New System.Drawing.Point(17, 193)
        Me.cancelBtn.Name = "cancelBtn"
        Me.cancelBtn.Size = New System.Drawing.Size(71, 28)
        Me.cancelBtn.TabIndex = 7
        Me.cancelBtn.Text = "Cancel"
        Me.cancelBtn.UseVisualStyleBackColor = True
        '
        'createBtn
        '
        Me.createBtn.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.createBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.createBtn.FlatAppearance.BorderSize = 2
        Me.createBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green
        Me.createBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.createBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.createBtn.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.createBtn.Location = New System.Drawing.Point(94, 193)
        Me.createBtn.Name = "createBtn"
        Me.createBtn.Size = New System.Drawing.Size(71, 28)
        Me.createBtn.TabIndex = 8
        Me.createBtn.Text = "Create"
        Me.createBtn.UseVisualStyleBackColor = True
        '
        'MasterListSelector
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(182, 231)
        Me.Controls.Add(Me.cancelBtn)
        Me.Controls.Add(Me.createBtn)
        Me.Controls.Add(Me.typesList)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "MasterListSelector"
        Me.Text = "Select Types"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents typesList As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cancelBtn As System.Windows.Forms.Button
    Private WithEvents createBtn As System.Windows.Forms.Button
End Class
