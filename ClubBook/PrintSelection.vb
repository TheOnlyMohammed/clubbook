﻿
Public Class PrintSelection
    Public currentStudents As New ArrayList
    Public selectedStudents As New ArrayList
    Dim studentDataFileName As String
    Dim returnValue As New ArrayList
    Dim file As New FileAnalyzer

    Dim textboxPlaceHolder = "Search using First or Last name."

    Private Sub PrintSelection_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        cancelBtn.PerformClick()
    End Sub


    Public Sub refreshListview()
        ListView1.Items.Clear()
        If file.numberDictionary.Count = 0 Then
            checkOptionsFile()
            file.readFile(studentDataFileName)
        End If

        'If selectedStudents.Count > 0 Then selectedStudents.Clear()

        For Each studentNumber As String In currentStudents
            ListView1.Items.Add(file.getNameFromNumber(studentNumber))
            If selectedStudents.Contains(studentNumber) Then
                ListView1.Items.Item(currentStudents.IndexOf(studentNumber)).Checked = True
            End If
        Next
        ListView1.CheckBoxes = True
        If ListView1.Items.Count = 0 Then
            ListView1.Items.Add("No members.")
            ListView1.CheckBoxes = False
        End If

    End Sub

    Private Sub checkOptionsFile()
        Dim reader As New System.IO.StreamReader(My.Computer.FileSystem.CurrentDirectory + "/options.moe")
        studentDataFileName = reader.ReadLine
        reader.Close()

        If My.Computer.FileSystem.FileExists(studentDataFileName) = False Then
            MsgBox("The student data file was not found! Please locate it and update the 'Options.moe' file.", MsgBoxStyle.Critical)
            Me.Close()
        End If
    End Sub

    Private Sub cancelBtn_Click(sender As Object, e As EventArgs) Handles cancelBtn.Click
        selectedStudents.Clear()
        selectedStudents.Add("cancel")
        Me.Hide()
    End Sub

    Private Sub selectAllBtn_Click(sender As Object, e As EventArgs) Handles selectAllBtn.Click
        For Each item As ListViewItem In ListView1.Items
            item.Checked = True
        Next
    End Sub

    Private Sub selectNoneBtn_Click(sender As Object, e As EventArgs) Handles selectNoneBtn.Click
        For Each item As ListViewItem In ListView1.Items
            item.Checked = False
            selectedStudents.Clear()
        Next
    End Sub
    Private Sub updateSelectedStudents()
        selectedStudents.Clear()
        For i As Integer = 0 To ListView1.Items.Count - 1
            If ListView1.Items(i).Checked = True Then
                selectedStudents.Add(currentStudents(i))
            End If
        Next
        returnValue = selectedStudents.Clone
    End Sub

    Private Sub printBtn_Click(sender As Object, e As EventArgs) Handles printBtn.Click
        If selectedStudents.Count > 0 Then selectedStudents.Clear()
        If ListView1.CheckedItems.Count = 0 Then
            MsgBox("Select students to print")
            Exit Sub
        End If
        updateSelectedStudents()

        Me.Hide()
    End Sub

    Private Sub TextBox1_GotFocus(sender As Object, e As EventArgs) Handles TextBox1.GotFocus
        If TextBox1.Text = textboxPlaceHolder Then
            TextBox1.Text = ""
            TextBox1.ForeColor = Color.Black
        End If

    End Sub


    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged

        searchTimer.Enabled = False
        searchTimer.Enabled = True

    End Sub

    Private Sub searchStudents()
        If TextBox1.Text = textboxPlaceholder Then Exit Sub
        If TextBox1.Text.Replace(" ", "") = "" Then
            Label1.Visible = True
            Label1.BringToFront()
            Me.Refresh()
            refreshListview()
            Label1.Visible = False
            Exit Sub
        End If

        updateSelectedStudents()
        ListView1.Items.Clear()
        ListView1.CheckBoxes = True
        Dim searchText As String = TextBox1.Text.ToLower
        Dim currentIndex As Integer = 0
        Dim counter As Integer = 0

        Label1.Visible = True
        Label1.BringToFront()
        Me.Refresh()
        For Each studentNumber As String In currentStudents

            'If the first name, last name or student number match the search then add them to the listview
            Dim student As ArrayList = file.numberDictionary(studentNumber)
            If file.studentMatchesSearchString(student, searchText) Then
                ListView1.Items.Add(file.getNameFromNumber(studentNumber))
                'Code to add more info is not needed
                '   student.RemoveAt(0)
                '   counter = 1
                '   For Each info In student
                '        ListView1.Items.Item(currentIndex).SubItems.Add(info.ToString)
                '       counter = counter + 1
                '       If counter = 4 Then Exit For
                '   Next

                If selectedStudents.Contains(studentNumber) Then
                    ListView1.Items(currentIndex).Checked = True
                    selectedStudents.Add(studentNumber)
                End If
                currentIndex = currentIndex + 1
            End If
        Next
        Label1.Visible = False
        If ListView1.Items.Count = 0 Then
            ListView1.Items.Add("No matches.")
            ListView1.CheckBoxes = False
        End If

    End Sub

    Private Sub searchTimer_Tick(sender As Object, e As EventArgs) Handles searchTimer.Tick

        searchStudents()
        searchTimer.Enabled = False
    End Sub

   
    Private Sub PrintSelection_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        TextBox1.Text = textboxPlaceHolder
        TextBox1.ForeColor = Color.Gray
        selectAllBtn.PerformClick()
    End Sub

End Class