﻿Public Class MasterListSelector
    Public returnValue As New ArrayList
    Private Sub MasterListSelector_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Public Sub fillListview(ByVal types As ArrayList)

        typesList.Items.Clear()

        For Each ty As String In types
            typesList.Items.Add(ty).Checked = True
        Next
        typesList.Columns(0).Width = typesList.Width - 3
    End Sub

    Private Sub cancelBtn_Click(sender As Object, e As EventArgs) Handles cancelBtn.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Hide()
    End Sub

    Private Sub createBtn_Click(sender As Object, e As EventArgs) Handles createBtn.Click
        If typesList.CheckedItems.Count = 0 Then
            MsgBox("Select at least one type to include in the master list.")
            Exit Sub
        End If
        returnValue.Clear()
        For Each item As ListViewItem In typesList.CheckedItems
            returnValue.Add(item.Text)
        Next
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Hide()
    End Sub
End Class