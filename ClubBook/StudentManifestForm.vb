﻿Public Class StudentManifestForm
    Public sportsName As String
    Public teacher As String
    Public results As New ArrayList From {}

    Private Sub StudentManifestForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        results.Clear()
        TextBox6.Text = sportsName
        TextBox3.Text = teacher
    End Sub

    Private Sub submitBtn_Click(sender As Object, e As EventArgs) Handles submitBtn.Click
       
        For i As Integer = 1 To 7
            Dim a As String = "TextBox" + Str(i)
            results.Add(Me.Controls(a.Replace(" ", "")).Text.Clone)
            Me.Controls(a.Replace(" ", "")).Text = ""
        Next
        results.Insert(2, datePicker.Value.ToShortDateString)
        datePicker.Value = Today.Date
        Me.DialogResult = Windows.Forms.DialogResult.Yes
        Me.Close()
    End Sub

    Private Sub cancelBtn_Click(sender As Object, e As EventArgs) Handles cancelBtn.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub


End Class