﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LoginScreen
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LoginScreen))
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.ListboxOptionsMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RenameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.addBtn = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.AddAccountTimer = New System.Windows.Forms.Timer(Me.components)
        Me.OptionsTimer = New System.Windows.Forms.Timer(Me.components)
        Me.renameBtn = New System.Windows.Forms.Button()
        Me.deleteBtn = New System.Windows.Forms.Button()
        Me.ListboxOptionsMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'ListBox1
        '
        Me.ListBox1.ContextMenuStrip = Me.ListboxOptionsMenu
        Me.ListBox1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 19
        Me.ListBox1.Items.AddRange(New Object() {"Mohammed", "Denys", "Test"})
        Me.ListBox1.Location = New System.Drawing.Point(40, 36)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(175, 232)
        Me.ListBox1.TabIndex = 0
        '
        'ListboxOptionsMenu
        '
        Me.ListboxOptionsMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeleteToolStripMenuItem, Me.RenameToolStripMenuItem})
        Me.ListboxOptionsMenu.Name = "ListboxOptionsMenu"
        Me.ListboxOptionsMenu.ShowImageMargin = False
        Me.ListboxOptionsMenu.Size = New System.Drawing.Size(93, 48)
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'RenameToolStripMenuItem
        '
        Me.RenameToolStripMenuItem.Name = "RenameToolStripMenuItem"
        Me.RenameToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.RenameToolStripMenuItem.Text = "Rename"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 24)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Accounts:"
        '
        'addBtn
        '
        Me.addBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.addBtn.FlatAppearance.BorderSize = 2
        Me.addBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray
        Me.addBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.addBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.addBtn.Location = New System.Drawing.Point(40, 305)
        Me.addBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.addBtn.Name = "addBtn"
        Me.addBtn.Size = New System.Drawing.Size(175, 33)
        Me.addBtn.TabIndex = 2
        Me.addBtn.Text = "Add Account"
        Me.addBtn.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(40, 279)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(175, 20)
        Me.TextBox1.TabIndex = 3
        Me.TextBox1.Text = "Enter Account Name"
        '
        'AddAccountTimer
        '
        Me.AddAccountTimer.Interval = 10
        '
        'OptionsTimer
        '
        Me.OptionsTimer.Interval = 10
        '
        'renameBtn
        '
        Me.renameBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.renameBtn.FlatAppearance.BorderSize = 2
        Me.renameBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Olive
        Me.renameBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Yellow
        Me.renameBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.renameBtn.Location = New System.Drawing.Point(40, 342)
        Me.renameBtn.Name = "renameBtn"
        Me.renameBtn.Size = New System.Drawing.Size(86, 23)
        Me.renameBtn.TabIndex = 4
        Me.renameBtn.Text = "Rename"
        Me.renameBtn.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.renameBtn.UseVisualStyleBackColor = True
        '
        'deleteBtn
        '
        Me.deleteBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.deleteBtn.FlatAppearance.BorderSize = 2
        Me.deleteBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkRed
        Me.deleteBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red
        Me.deleteBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.deleteBtn.Location = New System.Drawing.Point(134, 342)
        Me.deleteBtn.Name = "deleteBtn"
        Me.deleteBtn.Size = New System.Drawing.Size(86, 23)
        Me.deleteBtn.TabIndex = 4
        Me.deleteBtn.Text = "Delete"
        Me.deleteBtn.UseVisualStyleBackColor = True
        '
        'LoginScreen
        '
        Me.AcceptButton = Me.addBtn
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(259, 378)
        Me.Controls.Add(Me.addBtn)
        Me.Controls.Add(Me.deleteBtn)
        Me.Controls.Add(Me.renameBtn)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.TextBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "LoginScreen"
        Me.Text = "LoginScreen"
        Me.ListboxOptionsMenu.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents addBtn As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents AddAccountTimer As System.Windows.Forms.Timer
    Friend WithEvents ListboxOptionsMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RenameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OptionsTimer As System.Windows.Forms.Timer
    Friend WithEvents renameBtn As System.Windows.Forms.Button
    Friend WithEvents deleteBtn As System.Windows.Forms.Button
End Class
