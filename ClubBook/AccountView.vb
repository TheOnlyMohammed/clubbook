﻿Imports Excel = Microsoft.Office.Interop.Excel
Public Class AccountView
    'Misc Variables
    Public currentUser As String = " "
    Dim file As New FileAnalyzer
    Dim workPath As String = My.Computer.FileSystem.CurrentDirectory + "\"
    Dim studentDataFileName As String = "AutoStudentFile.txt"
    Dim editControls As New ArrayList
    Dim infoControls As New ArrayList
    Public currentStudents As New ArrayList
    Dim notes As New Dictionary(Of String, String)
    Dim types As New ArrayList From {"Arts", "Class", "Sport", "Personal Use", "Other Extracurriculars", "Other"}

    'Command Variables
    Dim keywords As New ArrayList From {"Add", "Delete", "Edit", "Print Attendance List", "Print Bus Manifest", "Print Photo Grid", _
                                           "Preview Attendance List", "Preview Bus Manifest", "Preview Photo Grid"}
    Dim help As New ArrayList From {"Help: Add <Club Name>",
                                    "Help: Delete <Club Name>",
                                    "Help: Edit <Club Name>",
                                    "Help: Rename <Old Club Name> , <New Club Name>",
                                    "Help: Print Attendance List <Club Name>",
                                    "Help: Print Bus Manifest <Club Name>",
                                    "Help: Print Photo Grid <Club Name>",
                                    "Help: Preview Attendance List <Club Name>",
                                    "Help: Preview Bus Manifest <Club Name>",
                                    "Help: Preview Photo Grid <Club Name>"}

    Dim shakeDuration As Integer = 500
    Dim shakeProgress As Integer = 0
    Dim shakeDistance As Integer = 7
    Dim moveTarget As Integer = 0
    Dim moveDistance As Integer = 5
    Dim moveDirection As Integer = 0

    'Printing Variables
    Dim pagesNeeded As Integer
    Dim studentsToPrint As New ArrayList
    Dim newPrintJob As Boolean = False
    Dim madeChanges As Boolean = False
    Dim printingInProgress As Boolean = False
    Dim picturesDirectory As String
    Dim currentPage As Integer
    Dim nameListTemp As New ArrayList
    Dim studentsToReport As New ArrayList
    Dim reports As New Dictionary(Of String, Dictionary(Of String, ArrayList))
    Dim studentManifestInfo As New ArrayList

    'Info panel Variables
    Dim infoTargetWidth As Integer
    Dim infoForcedWidth As Integer
    Dim startWidth As Integer
    Dim aniSpeed As Integer = 7
    Dim infoPanelShown As Boolean = False

    'Details checkbox Variables
    Dim detailsShown As Boolean = False

    Private Sub AccountView_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        CreateSelection.Close()
        StudentBrowser.Close()
        PrintSelection.Close()
        LoginScreen.Close()
        PrintPreviewDialog1.Close()

    End Sub


    Private Sub AccountView_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Escape Then

            If Not clubNameLbl.Text = "nclb" Then 'If its not a new club
                ListViewClubs.SelectedItems.Clear()
                hideInfoPanelContents()
                currentStudents.Clear()
                CreateToolStripMenuItem.Visible = False
                madeChanges = False
            Else
                ListViewClubs.SelectedItems.Clear()
                hideInfoPanelContents()
                currentStudents.Clear()
                membersList.Items.Clear()
                CreateToolStripMenuItem.Visible = False
                If ListViewClubs.Items(ListViewClubs.Items.Count - 1).Text = "New Club" Then
                    ListViewClubs.Items(ListViewClubs.Items.Count - 1).Remove()
                End If
                fillClubsListview()
                clubNameLbl.Text = ""
                madeChanges = False
            End If
            If detailsShown Then CheckboxAnimationTimer.Enabled = True
            If infoPanelShown Then
                infoPanelAnimationTimer.Enabled = True
                infoCheckbox.Checked = False
            End If
            'infoPanelAnimationTimer.Enabled = True
            If commandTxtbox.Location.Y > 0 Then
                commandMoveTimer.Enabled = True
                End
                commandErrorLabel.Visible = False
                e.Handled = False
                'ElseIf e.KeyCode = Keys.Space And e.Modifiers = Keys.Control Then
                '    commandMoveTimer.Enabled = True
            End If

        End If
    End Sub
#Region "Initialzing Functions"

    Private Sub AccountView_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        initializeForm()
    End Sub

    Public Sub initializeForm()
        infoTargetWidth = 981
        infoForcedWidth = infoTargetWidth
        Me.Text = currentUser + "'s Clubs"
        Me.Size = New Point(722, 520)
        moveTarget = Me.Height / 3 - commandTxtbox.Height
        commandTxtbox.Location = New Point(commandTxtbox.Location.X, commandTxtbox.Location.Y - 140)
        commandErrorLabel.Location = New Point(commandErrorLabel.Location.X, commandErrorLabel.Location.Y - 140)
        startWidth = 722
        infoCheckbox.SendToBack()

        'Hiding everything and showing label (looks better id it takes a long time to load
        InitLabel.Location = New Point((Me.Width - InitLabel.Width) / 2, (Me.Height - InitLabel.Height) / 2)
        For i As Integer = 0 To Me.Controls.Count - 1
            Me.Controls(i).Visible = False
        Next
        InitLabel.Visible = True

        'Checking options file
        checkOptionsFile()


        file.readFile(studentDataFileName)
        workPath = My.Computer.FileSystem.CurrentDirectory + "\" + currentUser + "\"

        'Initializing things
        initializeArrayLists()
        initializeCommands()
        initializeTypeSelector()
        fillClubsListview()
        hideInfoPanelContents()

        'Resetting info panel
        lastLbl.Text = ""
        firstLbl.Text = ""
        bornLbl.Text = ""
        numberLbl.Text = ""
        stuEmailLbl.Text = ""
        stuTeleLbl.Text = ""
        guardTeleLbl.Text = ""
        parentEmail1Lbl.Text = ""
        parentEmail2Lbl.Text = ""
        'noteLbl.Text = ""

        'Showing everything and hiding label
        For i As Integer = 0 To Me.Controls.Count - 1
            If Me.Controls(i).Name = "InitLabel" Then
                Me.Controls(i).Visible = False
            Else
                Me.Controls(i).Visible = True
            End If

        Next
    End Sub
    Private Sub initializeTypeSelector()
        typeSelector.Items.Clear()
        For Each type As String In types
            typeSelector.Items.Add(type)
        Next
    End Sub
    Private Sub initializeArrayLists()
        'Adding controls that are needed to edit clubs
        editControls.Add(clubNameTxt)
        editControls.Add(addStudentBtn)
        editControls.Add(removeStudentBtn)
        editControls.Add(addClassBtn)
        editControls.Add(Label1)
        editControls.Add(membersList)
        editControls.Add(editSaveBtn)
        editControls.Add(deleteBtn)
        editControls.Add(typeSelector)

        'Adding controls that are needed to view clubs
        infoControls.Add(Label1)
        infoControls.Add(clubNameLbl)
        infoControls.Add(membersList)
        infoControls.Add(editSaveBtn)
        infoControls.Add(typeLabel)
    End Sub

    Private Sub initializeCommands()
        commandTxtbox.AutoCompleteCustomSource.Clear()
        For Each helpStatment As String In help
            commandTxtbox.AutoCompleteCustomSource.Add(helpStatment)
        Next

        For Each keyowrd As String In keywords
            If keyowrd = "Add" Then Continue For

            For Each clubItem As ListViewItem In ListViewClubs.Items
                commandTxtbox.AutoCompleteCustomSource.Add(keyowrd + " " + clubItem.Text)
            Next

        Next

    End Sub

#End Region

    Private Sub ListViewClubs_KeyDown(sender As Object, e As KeyEventArgs) Handles ListViewClubs.KeyDown
        If e.KeyCode = Keys.Delete Or e.KeyCode = Keys.Back Then
            deleteClubBtn.PerformClick()
        End If
    End Sub


    Private Sub ListViewClubs_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListViewClubs.SelectedIndexChanged
        If ListViewClubs.SelectedIndices.Count > 0 Then
            Dim index As Integer = ListViewClubs.SelectedIndices(0)
            CreateToolStripMenuItem.Visible = True
            If madeChanges Then

                Dim result As MsgBoxResult
                If clubNameLbl.Text = "nclb" And Not ListViewClubs.SelectedItems(0).Text = "New Club" Then
                    result = MsgBox("Want to save your changes to this new club? (Any changes will be lost if you don't save.)", MsgBoxStyle.YesNoCancel, "Save Changes")
                ElseIf Not clubNameLbl.Text = ListViewClubs.SelectedItems(0).Text Then
                    result = MsgBox("Want to save your changes to " + clubNameLbl.Text + "? (Any changes will be lost if you don't save.)", MsgBoxStyle.YesNoCancel, "Save Changes")
                Else
                    result = MsgBoxResult.Cancel
                End If

                If result = MsgBoxResult.Yes Then
                    If validClub() Then
                        editSaveBtn.PerformClick()
                    Else
                        Exit Sub
                    End If
                ElseIf result = MsgBoxResult.Cancel Then
                    Exit Sub
                ElseIf result = MsgBoxResult.No And clubNameLbl.Text = "nclb" Then
                    ListViewClubs.Items(ListViewClubs.Items.Count - 1).Remove()
                End If

                madeChanges = False
            End If
            displayInfo(ListViewClubs.Items(index).Text)
        Else

            CreateToolStripMenuItem.Visible = False
        End If

    End Sub

    Private Sub NewClubToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NewClubToolStripMenuItem.Click
        addClubBtn.PerformClick()

    End Sub

    Private Sub clubNameTxt_GotFocus(sender As Object, e As EventArgs) Handles clubNameTxt.GotFocus
        If clubNameTxt.Text = "Enter Club Name" Then
            clubNameTxt.Text = ""
        End If
        madeChanges = True
    End Sub


    Private Sub RefreshClubsListToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RefreshClubsListToolStripMenuItem.Click
        fillClubsListview()

    End Sub

    Private Sub meetDay_SelectedIndexChanged(sender As Object, e As EventArgs)
        madeChanges = True
    End Sub

    Private Sub clubNameTxt_KeyDown(sender As Object, e As KeyEventArgs) Handles clubNameTxt.KeyDown
        If e.KeyCode = Keys.Enter Then
            editSaveBtn.PerformClick()
        End If
    End Sub

    Private Sub BackToLoginScreenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BackToLoginScreenToolStripMenuItem.Click
        LoginScreen.Show()
        Me.Hide()
    End Sub

#Region "Custom Functions"

    Private Sub checkOptionsFile()
        Dim reader As New System.IO.StreamReader(My.Computer.FileSystem.CurrentDirectory + "/options.moe")
        studentDataFileName = reader.ReadLine
        picturesDirectory = reader.ReadLine
        If Not picturesDirectory.Last = "/" Or Not picturesDirectory.Last = "\" Then
            picturesDirectory = picturesDirectory + "\"
        End If
        reader.Close()

        If My.Computer.FileSystem.FileExists(studentDataFileName) = False Then
            MsgBox("The student data file was not found! Please locate it and update the 'Options.moe' file.", MsgBoxStyle.Critical)
            Me.Close()
        End If
    End Sub

    Private Function checkEmpty(ByVal input As String) As String
        If input = "" Then
            input = "None"
        End If
        Return input
    End Function
    Private Sub displayInfo(ByVal clubName As String)
        If clubName = "There are no clubs." Then
            Exit Sub
        End If
        If clubName = "New Club" Then
            showEditControls()
            clubNameLbl.Text = "nclb"
            totalMembers.Text = "(0 total)"
            Exit Sub
        End If
        showInfoControls()
        clubNameLbl.Text = clubName
        totalMembers.Text = "(" + currentStudents.Count.ToString + " total)"

        Dim reader As New System.IO.StreamReader(workPath + clubName + ".txt")
        'Filling members list

        Dim type As String = reader.ReadLine
        Dim tempLine As String

        'Getting type and checking validity
        If validType(type) = False Then
            tempLine = type
            showEditControls()
            clubNameTxt.Text = clubName
            typeSelector.Text = "Select a type"
            MsgBox("This club does not have a valid type assigned to it. Please assign it a valid type and save it.")
        Else
            tempLine = reader.ReadLine
            typeLabel.Text = "(" + file.capitalize(type.Trim(" ").ToLower) + ")"
            typeLabel.Location = New Point(clubNameLbl.Location.X + clubNameLbl.Width, clubNameLbl.Location.Y)
        End If
        


        membersList.Items.Clear()
        currentStudents.Clear()
        notes.Clear()
        'Making sure first line is valid
        If Not tempLine = "" Then
            currentStudents.Add(tempLine.Split("|").GetValue(0))
            If tempLine.Contains("|") And Not tempLine.Split("|").GetValue(1).ToString.Replace(" ", "") = "" Then
                notes.Add(tempLine.Split("|").GetValue(0), tempLine.Split("|").GetValue(1))
            End If
        End If

        While reader.EndOfStream = False
            tempLine = reader.ReadLine
            currentStudents.Add(tempLine.Split("|").GetValue(0))
            If tempLine.Contains("|") And Not tempLine.Split("|").GetValue(1).ToString.Replace(" ", "") = "" Then
                notes.Add(tempLine.Split("|").GetValue(0), tempLine.Split("|").GetValue(1))
            End If
        End While
        reader.Close()

        updateMembersList()
        If membersList.Items.Count = 0 Then
            membersList.Items.Add("There are no members in this club.")
        End If
        CreateToolStripMenuItem.Visible = True

    End Sub
    Public Sub updateMembersList()
        membersList.Items.Clear()
        Dim problem As Boolean = False
        Dim problemIndexs As New ArrayList
        Dim counter As Integer = 0

        'Deleting students who are gone
        For Each studentNumber As String In currentStudents
            If file.getNameFromNumber(studentNumber) = "NTFOUND!" Then
                problemIndexs.Add(counter)
                problem = True
            End If
            counter = counter + 1
        Next

        Dim deleted As Integer = 0
        If problem Then
            For Each problemIndex As Integer In problemIndexs
                currentStudents.RemoveAt(problemIndex - deleted)
                deleted = deleted + 1
            Next
            saveChanges(True)
        End If


        'Alphabetizing the list
        Dim alphaList As New ArrayList
        Dim curName As String = ""
        For Each studentNumber As String In currentStudents
            curName = file.getAttendanceNameFromNumber(studentNumber)
            If Not curName = "NTFOUND!" Then
                alphaList.Add(curName + studentNumber)
            End If
        Next
        alphaList.Sort()
        currentStudents.Clear()
        For Each n As String In alphaList
            currentStudents.Add(n.Substring(n.Length - 9))
        Next

        'Adding names and notes to list
        For Each studentNumber As String In currentStudents
            Dim name As String = file.getNameFromNumber(studentNumber)
            membersList.Items.Add(name + getNote(studentNumber, True))
            counter = counter + 1
        Next


        totalMembers.Text = "(" + currentStudents.Count.ToString + " total)"
        If deleted > 0 Then
            MsgBox(Str(deleted) + " student(s) were deleted from this club because they no longer attend this school.", MsgBoxStyle.OkOnly)
        End If
    End Sub
    Private Sub showInfoControls()
        For Each item As Control In infoGroup.Controls
            If editControls.Contains(item) And infoControls.Contains(item) = False Then
                item.Visible = False
            Else
                item.Visible = True
            End If
        Next
        editSaveBtn.Text = "Edit"

    End Sub

    Private Sub showEditControls()
        For Each item As Control In infoGroup.Controls
            If infoControls.Contains(item) And editControls.Contains(item) = False Then
                item.Visible = False
            Else
                item.Visible = True
            End If
        Next
        editSaveBtn.Text = "Save"
        madeChanges = False
    End Sub

    Private Sub fillClubsListview()
        ListViewClubs.Clear()
        ListViewClubs.Columns.Add("Clubs")
        ListViewClubs.Columns(ListViewClubs.Columns.Count - 1).Width = ListViewClubs.Width - 5
        ListViewClubs.Columns(0).TextAlign = HorizontalAlignment.Center

        Dim files = My.Computer.FileSystem.GetFiles(workPath)
        For Each file As String In files
            file = file.Replace(workPath, " ").Substring(1)
            file = file.Remove(file.Length - 4)
            ListViewClubs.Items.Add(file)

        Next

        If ListViewClubs.Items.Count = 0 Then
            ListViewClubs.Items.Add("There are no clubs.")
        End If
    End Sub

    Private Sub hideInfoPanelContents()
        For Each item As Control In infoGroup.Controls
            If item.Name = "tempLabel" Then Continue For
            item.Visible = False
        Next

    End Sub
    Private Function validClub()
        'If they inputed a valid name
        If clubNameTxt.Text.ToLower = "enter club name" Or clubNameTxt.Text.Replace(" ", "") = "" Or clubNameTxt.Text.ToLower = "new club" Then
            MsgBox("Enter a valid name for the club.")
            Return False
        End If

        'If the file already exists and the name has changed
        If My.Computer.FileSystem.FileExists(workPath + clubNameTxt.Text + ".txt") And Not clubNameLbl.Text = clubNameTxt.Text Then
            MsgBox("There is already a club with the name '" + clubNameTxt.Text + "'")
            Return False
        End If

        Return True
    End Function
    Private Function getNote(ByVal key As String, Optional ByVal brackets As Boolean = False)
        If notes.ContainsKey(key) Then
            Return IIf(brackets, "  (" + notes(key) + ")", notes(key))
        Else
            Return ""
        End If
    End Function
    Private Sub saveChanges(Optional ByVal forcedSave As Boolean = False)
        'If its is a force save then dont check if input is valid
        If Not forcedSave Then If Not validClub() Then Exit Sub

        If file.isValidInput(clubNameTxt.Text, "\", "/", ":", "?", """", ">", "<", "|") = False Then
            MsgBox("A club name cannot contain: \ / : * ? "" < > |")
            Exit Sub
        End If

        If types.Contains(typeSelector.Text) = False Then
            MsgBox("Please select a valid type for this club.")
            Exit Sub
        End If

        If currentStudents.Count = 0 Then
            MsgBox("Please add at least one student to this club.")
            Exit Sub
        End If

        'If its not a new club then delete the old file
        If Not clubNameLbl.Text = "nclb" And Not clubNameTxt.Text = clubNameLbl.Text Then
            My.Computer.FileSystem.DeleteFile(workPath + clubNameLbl.Text + ".txt")
        End If
        'If its a force save then use the lbl name not the textbox name
        Dim clubName As String = IIf(forcedSave, clubNameLbl.Text, clubNameTxt.Text)
        clubName = clubName.Trim(" ")
        'if its a force save then use the label text not the dropdown
        Dim type As String = IIf(forcedSave, typeLabel.Text.Replace("(", "").Replace(")", ""), typeSelector.Text)

        Dim writer As New System.IO.StreamWriter(workPath + file.capitalize(clubName) + ".txt")
        Dim finalFile As String = type + vbNewLine

        If currentStudents.Count > 0 Then

            'finalFile = currentStudents(0) + "|" + getNote(currentStudents(0))
            'currentStudents.RemoveAt(0)
            'Adding the student numbers
            For Each studentNumber As String In currentStudents
                finalFile = finalFile + studentNumber + "|" + getNote(studentNumber) + vbNewLine
            Next
        End If

        writer.Write(finalFile)
        writer.Close()

        If Not forcedSave Then
            displayInfo(clubName)
            fillClubsListview()
            initializeCommands()
            madeChanges = False
        End If
    End Sub
#End Region

#Region "Button Code"
    Private Sub editSaveBtn_Click(sender As Object, e As EventArgs) Handles editSaveBtn.Click
        deleteBtn.Text = "Delete"
        If editSaveBtn.Text = "Edit" Then
            showEditControls()
            clubNameTxt.Text = clubNameLbl.Text
            Dim type As String = typeLabel.Text.Replace("(", "").Replace(")", "")
            If validType(type) Then
                typeSelector.Text = type
            Else
                MsgBox(type + " is not a valid type, please choose a valid one from the dropdown.")
            End If

        ElseIf editSaveBtn.Text = "Save" Then
            saveChanges()
        End If

    End Sub

    Private Function validType(ByVal type As String)
        Return types.Contains(file.capitalize(type.Trim(" ").ToLower))
    End Function
    Private Sub addStudentBtn_Click(sender As Object, e As EventArgs) Handles addStudentBtn.Click
        StudentBrowser.selectedStudents = currentStudents.Clone
        StudentBrowser.Button1.Text = "Add Students"
        StudentBrowser.layoutListView()
        StudentBrowser.Text = "Members of " + clubNameTxt.Text
        StudentBrowser.ShowDialog()

        If StudentBrowser.DialogResult = Windows.Forms.DialogResult.Cancel Then Exit Sub
        currentStudents = StudentBrowser.selectedStudents.Clone
        currentStudents = removeDuplicates(currentStudents)
        updateMembersList()
        madeChanges = True
    End Sub
    Private Function removeDuplicates(ByVal list As ArrayList)
        Dim newList As New ArrayList
        For i As Integer = 0 To list.Count - 1
            If newList.Contains(list(i)) = False Then
                newList.Add(list(i))
            End If
        Next
        Return newList.Clone
    End Function
    Private Sub removeStudentBtn_Click(sender As Object, e As EventArgs) Handles removeStudentBtn.Click
        If membersList.SelectedIndex = -1 Then
            MsgBox("Select the student(s) to delete")
        End If

        Dim selectedIndex As Integer
        Dim studentsDeleted As Integer = 0
        'Deletes the currentely selected items
        While membersList.SelectedItems.Count > 0
            selectedIndex = membersList.SelectedIndex
            currentStudents.RemoveAt(selectedIndex - studentsDeleted)
            studentsDeleted = studentsDeleted + 1
            membersList.SetSelected(selectedIndex, False)
        End While
        updateMembersList()
        madeChanges = True
    End Sub

    Private Sub deleteBtn_Click(sender As Object, e As EventArgs) Handles deleteBtn.Click
        If clubNameLbl.Text = "nclb" Then
            ListViewClubs.SelectedItems.Clear()
            hideInfoPanelContents()
            currentStudents.Clear()
            membersList.Items.Clear()
            CreateToolStripMenuItem.Visible = False
            If ListViewClubs.Items(ListViewClubs.Items.Count - 1).Text = "New Club" Then
                ListViewClubs.Items(ListViewClubs.Items.Count - 1).Remove()
            End If
            fillClubsListview()
            clubNameLbl.Text = ""
            madeChanges = False
            Exit Sub
        End If
        If MsgBox("Are you sure you want to delete this club? (Cannot be undone!)", MsgBoxStyle.YesNo, "Delete Club") = MsgBoxResult.Yes Then
            My.Computer.FileSystem.DeleteFile(workPath + clubNameLbl.Text + ".txt")
            hideInfoPanelContents()
            fillClubsListview()
            initializeCommands()
        End If
    End Sub

    Private Sub addClub_Click(sender As Object, e As EventArgs) Handles addClubBtn.Click
        If ListViewClubs.Items(0).Text = "There are no clubs." Then
            ListViewClubs.Items(0).Remove()
        End If
        If clubNameLbl.Text = "nclb" Then
            MsgBox("You can only create one new club at a time.")
            Exit Sub
        End If
        ListViewClubs.Items.Add("New Club")
        deleteBtn.Text = "Cancel"
        For Each item As ListViewItem In ListViewClubs.Items
            item.Selected = False
        Next
        ListViewClubs.Items(ListViewClubs.Items.Count - 1).Selected = True
        clubNameTxt.Text = "Enter Club Name"
        membersList.Items.Clear()
        currentStudents.Clear()
        clubNameLbl.Text = "nclb"
        typeSelector.Text = "Select a type"
        madeChanges = True

    End Sub

    Private Sub deleteClubBtn_Click(sender As Object, e As EventArgs) Handles deleteClubBtn.Click
        If Not ListViewClubs.SelectedItems.Count = 1 Then
            If ListViewClubs.SelectedItems.Count > 1 Then
                MsgBox("You cannot delete more than one club at once!")
                Exit Sub
            Else
                MsgBox("Please select a club to delete.")
                Exit Sub
            End If
        End If
        If ListViewClubs.SelectedItems(0).Text = "There are no clubs." Then
            Exit Sub
        End If
        deleteBtn.Visible = True
        deleteBtn.PerformClick()

    End Sub
#End Region

#Region "Printing Code"

    Private Sub PhotoGrid_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PhotoGrid.PrintPage
        '(650,900)

        Dim imageScaleFactor As Single = 0.85
        Dim xSpacing As Integer = 5
        Dim cellWidth As Integer = 150.0 * imageScaleFactor + xSpacing
        Dim cellHeight As Integer = 190.0 * imageScaleFactor + e.Graphics.MeasureString("The Quick Brown fox jumped over the lazy dog", Font).Height + 2 * xSpacing
        Dim gridWidth As Integer = CInt(e.MarginBounds.Width / cellWidth)
        Dim gridHeight As Integer = CInt(e.MarginBounds.Height / cellHeight)
        Dim gridArea As Integer = gridHeight * gridWidth

        'If its a new job then initialize the varibles
        If newPrintJob Then
            newPrintJob = False
            pagesNeeded = CInt(Math.Ceiling(studentsToPrint.Count / CDec(gridArea)))
            'studentsToPrint = currentStudents.Clone
        End If

        If pagesNeeded > 1 Then
            printPhotoGrid(e, 0, gridArea, cellWidth, cellHeight, imageScaleFactor)
            e.HasMorePages = True
            studentsToPrint.RemoveRange(0, gridArea)
            pagesNeeded = pagesNeeded - 1
        ElseIf pagesNeeded = 1 Then
            printPhotoGrid(e, 0, studentsToPrint.Count, cellWidth, cellHeight, imageScaleFactor)
            pagesNeeded = 0
            studentsToPrint.Clear()
            e.HasMorePages = False
        End If



    End Sub

    Private Sub printPhotoGrid(e As Printing.PrintPageEventArgs, ByVal startIndex As Integer, ByVal count As Integer, ByVal cellWidth As Integer, _
                        ByVal cellHeight As Integer, ByVal imageScaleFactor As Single)

        Dim counter As Integer = 0
        Dim font As New Font("Arial", 9)
        Dim titleFont As New Font("Times New Roman", 20, FontStyle.Bold)
        Dim x As Integer
        Dim y As Integer
        Dim textWidth As Integer
        Dim name As String
        Dim pageStudents As New ArrayList
        pageStudents = studentsToPrint.GetRange(startIndex, count).Clone

        'MsgBox((e.MarginBonds.Width - e.PageBounds.Width) * 0.75 / 2)
        e.Graphics.DrawString(clubNameLbl.Text, titleFont, Brushes.Black, (e.MarginBounds.Width - e.PageBounds.Width) * 0.25, (e.MarginBounds.Height - e.PageBounds.Height) * 0.25)
        e.Graphics.DrawString(currentUser, titleFont, Brushes.Black, e.MarginBounds.Width - (e.Graphics.MeasureString(currentUser, titleFont).Width + 0.25 * (e.MarginBounds.Width - e.PageBounds.Width)), (e.MarginBounds.Height - e.PageBounds.Height) * 0.25)
        PhotoGrid.OriginAtMargins = True
        For row As Integer = 0 To CInt(e.MarginBounds.Height / cellHeight) - 1
            For column As Integer = 0 To CInt(e.MarginBounds.Width / cellWidth) - 1
                If counter = pageStudents.Count Then
                    Exit For
                End If

                Dim imagePath As String = picturesDirectory + pageStudents(counter) + ".bmp"

                If My.Computer.FileSystem.FileExists(imagePath) = False Then
                    imagePath = My.Computer.FileSystem.CurrentDirectory + "/NotFound.bmp"
                End If

                Dim image As New Bitmap(imagePath)

                x = cellWidth * column
                y = cellHeight * row
                e.Graphics.DrawImage(image, x, y, CInt(150 * imageScaleFactor), CInt(190 * imageScaleFactor))
                name = file.getNameFromNumber(pageStudents(counter))
                textWidth = e.Graphics.MeasureString(name, font).Width
                If textWidth > 150.0 * imageScaleFactor Then
                    name = name.Split(" ").GetValue(0)
                    textWidth = e.Graphics.MeasureString(name, font).Width
                End If
                e.Graphics.DrawString(name, font, Brushes.Black, cellWidth * column + ((150.0 * imageScaleFactor) - textWidth) / 2, y + 190 * imageScaleFactor)
                counter = counter + 1

            Next
            If counter = pageStudents.Count Then
                Exit For
            End If
        Next

    End Sub

    Private Sub NameList_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles NameList.PrintPage

        Dim namesPerPage As Integer = 40

        'If its a new job then initialize the varibles
        If newPrintJob Then
            newPrintJob = False
            pagesNeeded = Math.Ceiling(studentsToPrint.Count / namesPerPage)
            nameListTemp.Clear()
            For Each studentNumber As String In studentsToPrint
                nameListTemp.Add(file.getAttendanceNameFromNumber(studentNumber))
            Next
            studentsToPrint.Clear()
            studentsToPrint = nameListTemp.Clone
            studentsToPrint.Sort()
            currentPage = 0
        End If

        If pagesNeeded > 1 Then
            printNameList(e, namesPerPage)
            e.HasMorePages = True
            studentsToPrint.RemoveRange(0, namesPerPage)
            pagesNeeded = pagesNeeded - 1
            currentPage = currentPage + 1
        ElseIf pagesNeeded = 1 Then
            printNameList(e, namesPerPage)
            pagesNeeded = 0
            currentPage = 0
            studentsToPrint.Clear()
            e.HasMorePages = False
        End If


    End Sub

    Private Sub printNameList(e As Printing.PrintPageEventArgs, ByVal namesPerPage As Integer)

        'Declaring all Variables that are not dynamic
        Dim columns As Integer = 25
        Dim majorLines As Integer = 5
        Dim majorLinePen As New Pen(Brushes.Black, 3)
        Dim minorLinePen As New Pen(Brushes.Black, 1)
        Dim height As Integer = e.PageBounds.Height
        Dim width As Integer = e.PageBounds.Width
        Dim startHeight As Integer = 85
        Dim startWidth As Integer = 230
        Dim xMargin As Integer = 30
        Dim yMargin As Integer = 35
        Dim font As New Font("Arial", 11)
        Dim titleFont As New Font("Times new roman", 20, FontStyle.Bold)
        Dim subTitleFont As New Font("Times new roman", 14)
        While e.Graphics.MeasureString(clubNameLbl.Text, titleFont).Width >= startWidth
            titleFont = New Font(titleFont.Name, titleFont.Size - 1)
        End While

        'Delcaring Rest of Variables
        Dim longest As Integer = 0
        Dim tempWidth As Integer = 0
        For nameIndex As Integer = 0 To namesPerPage - 1 'Making the name column as long as the longest name
            If nameIndex = studentsToPrint.Count Then Exit For
            tempWidth = e.Graphics.MeasureString(studentsToPrint(nameIndex) + " " + getNote(file.getNumberFromAttendanceName(currentStudents, studentsToPrint(nameIndex)), True), font).Width
            If tempWidth > longest Then longest = tempWidth
        Next
        startWidth = longest + xMargin * 2 + 10
        Dim xSpacing As Integer = (width - xMargin * 2 - startWidth) / columns
        Dim ySpacing As Integer = (height - startHeight - yMargin) / namesPerPage
        Dim horizontalLineRight As Integer = startWidth + columns * xSpacing

        Dim borderRect As New Rectangle(xMargin, startHeight, startWidth + columns * xSpacing - xMargin, ySpacing * namesPerPage)

        'Drawing Titles
        e.Graphics.DrawString(clubNameLbl.Text, titleFont, Brushes.Black, xMargin + ((startWidth - xMargin) - e.Graphics.MeasureString(clubNameLbl.Text, titleFont).Width) / 2, 10)
        e.Graphics.DrawString(currentUser, subTitleFont, Brushes.Black, xMargin + ((startWidth - xMargin) - e.Graphics.MeasureString(currentUser, subTitleFont).Width) / 2, e.Graphics.MeasureString(clubNameLbl.Text, titleFont).Height + 8)
        Dim dateString As String = MonthName(Today.Month, True) + " " + Today.Day.ToString + ", " + Today.Year.ToString

        e.Graphics.DrawString(dateString, font, Brushes.Black, xMargin + ((startWidth - xMargin) - e.Graphics.MeasureString(dateString, font).Width) / 2, startHeight - e.Graphics.MeasureString(dateString, font).Height)
        'Drawing GridLines

        e.Graphics.DrawRectangle(minorLinePen, borderRect)

        'Drawing Columns
        For col As Integer = 0 To columns
            e.Graphics.DrawLine(IIf((col Mod majorLines) = 0, majorLinePen, minorLinePen), startWidth + col * xSpacing, startHeight, startWidth + col * xSpacing, borderRect.Bottom)
            e.Graphics.DrawLine(IIf((col Mod majorLines) = 0, majorLinePen, minorLinePen), startWidth + col * xSpacing, startHeight, startWidth + (col + 1) * xSpacing, 10)
        Next

        'Drawing Rows
        e.Graphics.DrawLine(majorLinePen, xMargin, startHeight, xMargin, borderRect.Bottom)
        For row As Integer = 0 To namesPerPage
            e.Graphics.DrawLine(IIf((row Mod majorLines) = 0, majorLinePen, minorLinePen), xMargin, startHeight + ySpacing * row, horizontalLineRight, startHeight + ySpacing * row)
        Next

        'Drawing Names

        Dim textMargin As Integer = xMargin * 1.5
        For nameRow As Integer = 0 To namesPerPage - 1
            If nameRow = studentsToPrint.Count Then Exit For
            Dim studentName As String = (currentPage * namesPerPage + nameRow + 1).ToString + ". " + studentsToPrint(nameRow) + " " + getNote(file.getNumberFromAttendanceName(currentStudents, studentsToPrint(nameRow)), True)
            If nameRow >= 9 Then textMargin = CInt(xMargin * 1.5) - e.Graphics.MeasureString(studentName.Chars(0), font).Width
            e.Graphics.DrawString(studentName, font, Brushes.Black, textMargin, startHeight + ySpacing * (nameRow + 1.2) - e.Graphics.MeasureString(studentName, font).Height - 3)
        Next
    End Sub

    Private Sub BusManifest_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles BusManifest.PrintPage
        Dim namesPerPage As Integer = 48

        'If its a new job then initialize the varibles
        If newPrintJob Then
            newPrintJob = False
            pagesNeeded = Math.Ceiling(studentsToPrint.Count / namesPerPage)
            
            If StudentManifestForm.DialogResult = Windows.Forms.DialogResult.Cancel Or studentManifestInfo.Count = 0 Then
                studentsToPrint.Clear()
                PrintPreviewDialog1.Close()
                Exit Sub
            End If

        End If

        If pagesNeeded > 1 Then
            printBusManifestPage(e)
            e.HasMorePages = True
            pagesNeeded = pagesNeeded - 1
        ElseIf pagesNeeded = 1 Then
            printBusManifestPage(e)
            pagesNeeded = 0
            studentsToPrint.Clear()
            e.HasMorePages = False
        End If

    End Sub

    Private Sub printBusManifestPage(e As Printing.PrintPageEventArgs)
        'Dividing by three is nessesary, it has something to do with scale. 
        'Im not sure how i found out it was 3 but it works.

        Dim scalingFactor As Integer = 3
        Dim startX As Integer = 413 / scalingFactor + 5
        Dim startY As Integer = 1509 / scalingFactor
        Dim nameColWidth As Integer = 550 / scalingFactor
        Dim telephoneOffset As Integer = 597 / scalingFactor
        Dim telephoneColWidth As Integer = 275 / scalingFactor
        Dim groupSpacing As Integer = 1145 / scalingFactor
        Dim ySpacing As Double = 58.0 / scalingFactor

        Dim defaultFontSize As Integer = 10
        Dim font As New Font("Arial", defaultFontSize)

        e.Graphics.DrawImage(New Bitmap("StudentManifest.bmp"), 0, 0)

        'Filling in other info

        'Format is:
        '..DrawString(text, correctly sized font using "getAppropiateFont" function, Color: black, startX of line / scaling factor , startY of line / scaling factor - height of text)
        'Quick python program for making it easier to create the code below
        '    While True
        '        p = Input("index ")
        '        w = Input("width ")
        '        x = Input("x ")
        '        y = Input("y ")
        '       print "e.Graphics.DrawString(studentManifestInfo(%i), getAppropiateFont(font, studentManifestInfo(%i), %i / scalingFactor, e), Brushes.Black, %i / scalingFactor, %i / scalingFactor - e.Graphics.MeasureString(studentManifestInfo(%i), getAppropiateFont(font, studentManifestInfo(%i), %i / scalingFactor, e)).Height)" %(p,p,w,x,y,p,p,w)


        'Event Name
        e.Graphics.DrawString(studentManifestInfo(0), getAppropiateFont(font, studentManifestInfo(0), 1390 / scalingFactor, e), Brushes.Black, 1000 / scalingFactor, 495 / scalingFactor - e.Graphics.MeasureString(studentManifestInfo(0), getAppropiateFont(font, studentManifestInfo(0), 1390 / scalingFactor, e)).Height)

        'Event Location
        e.Graphics.DrawString(studentManifestInfo(1), getAppropiateFont(font, studentManifestInfo(1), 1140 / scalingFactor, e), Brushes.Black, 1255 / scalingFactor, 595 / scalingFactor - e.Graphics.MeasureString(studentManifestInfo(1), getAppropiateFont(font, studentManifestInfo(1), 1140 / scalingFactor, e)).Height)

        'Event Date
        e.Graphics.DrawString(studentManifestInfo(2), getAppropiateFont(font, studentManifestInfo(2), 433 / scalingFactor, e), Brushes.Black, 261 / scalingFactor, 600 / scalingFactor - e.Graphics.MeasureString(studentManifestInfo(2), getAppropiateFont(font, studentManifestInfo(2), 433 / scalingFactor, e)).Height)

        'Teacher in Charge
        e.Graphics.DrawString(studentManifestInfo(3), getAppropiateFont(font, studentManifestInfo(3), 936 / scalingFactor, e), Brushes.Black, 507 / scalingFactor, 1210 / scalingFactor - e.Graphics.MeasureString(studentManifestInfo(3), getAppropiateFont(font, studentManifestInfo(3), 936 / scalingFactor, e)).Height)

        'Teacher cell phone
        e.Graphics.DrawString(studentManifestInfo(4), getAppropiateFont(font, studentManifestInfo(4), 388 / scalingFactor, e), Brushes.Black, 1796 / scalingFactor, 1209 / scalingFactor - e.Graphics.MeasureString(studentManifestInfo(4), getAppropiateFont(font, studentManifestInfo(4), 388 / scalingFactor, e)).Height)

        'Class code
        e.Graphics.DrawString(studentManifestInfo(5), getAppropiateFont(font, studentManifestInfo(5), 800 / scalingFactor, e), Brushes.Black, 416 / scalingFactor, 700 / scalingFactor - e.Graphics.MeasureString(studentManifestInfo(5), getAppropiateFont(font, studentManifestInfo(5), 800 / scalingFactor, e)).Height)

        'Group Name
        e.Graphics.DrawString(studentManifestInfo(6), getAppropiateFont(font, studentManifestInfo(6), 818 / scalingFactor, e), Brushes.Black, 1560 / scalingFactor, 700 / scalingFactor - e.Graphics.MeasureString(studentManifestInfo(6), getAppropiateFont(font, studentManifestInfo(6), 818 / scalingFactor, e)).Height)

        'Event Time
        e.Graphics.DrawString(studentManifestInfo(7), getAppropiateFont(font, studentManifestInfo(7), 360 / scalingFactor, e), Brushes.Black, 708 / scalingFactor, 595 / scalingFactor - e.Graphics.MeasureString(studentManifestInfo(7), getAppropiateFont(font, studentManifestInfo(7), 360 / scalingFactor, e)).Height)

        'Filling in rows of names and telephone numbers
        Dim adjustedSize As Integer = defaultFontSize
        'Printing first column
        For i = 0 To 23
            If studentsToPrint.Count = 0 Then Exit For

            Dim name As String = file.getNameFromNumber(studentsToPrint(0))
            e.Graphics.DrawString(name, getAppropiateFont(font, name, nameColWidth, e), Brushes.Black, startX, startY + i * ySpacing) '- e.Graphics.MeasureString(studentsToPrint(0), font).Height
            Dim telephone As String = IIf(file.numberDictionary(studentsToPrint(0)).Item(7) = "", file.numberDictionary(studentsToPrint(0)).Item(8), file.numberDictionary(studentsToPrint(0)).Item(7))
            e.Graphics.DrawString(telephone, getAppropiateFont(font, telephone, telephoneColWidth, e), Brushes.Black, startX + telephoneOffset, startY + 5 + i * ySpacing)
            studentsToPrint.RemoveAt(0)
        Next

        'Printing Second column
        If studentsToPrint.Count >= 1 Then
            For i = 0 To 23


                Dim name As String = file.getNameFromNumber(studentsToPrint(0))
                e.Graphics.DrawString(name, getAppropiateFont(font, name, nameColWidth, e), Brushes.Black, startX + groupSpacing, startY + i * ySpacing)
                Dim telephone As String = IIf(file.numberDictionary(studentsToPrint(0)).Item(7) = "", file.numberDictionary(studentsToPrint(0)).Item(8), file.numberDictionary(studentsToPrint(0)).Item(7))
                e.Graphics.DrawString(telephone, getAppropiateFont(font, telephone, telephoneColWidth, e), Brushes.Black, startX + groupSpacing + telephoneOffset, startY + 5 + i * ySpacing)
                studentsToPrint.RemoveAt(0)
                If studentsToPrint.Count = 0 Then Exit For
            Next
        End If
    End Sub

    Private Sub EligForm_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles EligForm.PrintPage
        Dim scalingFactor = 1.5 ' This is needed because the measurements produced by photoshop are not to scale
        Dim columnWidths As New ArrayList From {215, 175, 102, 120, 80, 90, 75, 167}
        Dim columnHeight As Integer = 30 / scalingFactor
        Dim startX As Integer = 151 / scalingFactor
        Dim startY As Integer = 342 / scalingFactor
        Dim textPadding As Integer = 3
        Dim defaultFontSize As Integer = 10
        Dim font As New Font("Arial", 8, FontStyle.Bold)
        e.Graphics.DrawImage(New Bitmap("EligibilityForm.bmp"), 0, 0)

        Dim headerX As Integer = 646 / scalingFactor
        Dim headerY As Integer = 300 / scalingFactor
        Dim headerHeight As Integer = 40 / scalingFactor
        Dim headerWidth As Integer = 117 / scalingFactor

        'Filling in header
        e.Graphics.DrawString("AGE AT", font, Brushes.Black, headerX + getCenteredX(headerWidth, "AGE AT", font, e), headerY + 1)
        e.Graphics.DrawString("JAN 1, " + getSchoolYear(), font, Brushes.Black, headerX + getCenteredX(headerWidth, "JAN 1, " + getSchoolYear(), font, e), headerY + headerHeight / 2)

        'Declaring info needed to fill sheet
        Dim studentInfo As New ArrayList
        Dim firstName As String
        Dim lastName As String
        Dim gender As String
        Dim bDay As String
        Dim bMonth As String
        Dim bYear As String
        Dim age As String
        Dim gradeEntry As String

        font = New Font("Arial", defaultFontSize)



        For row = 0 To 29
            If studentsToPrint.Count = 0 Then Exit For

            studentInfo = file.numberDictionary(studentsToPrint(0)).Clone

            firstName = checkEmpty(studentInfo(1))
            lastName = checkEmpty(studentInfo(0))
            gender = IIf(studentInfo(2).ToString.ToUpper = "M", "Male", "Female")
            bDay = studentInfo(6).ToString.Substring(6, 2)
            bMonth = studentInfo(6).ToString.Substring(4, 2)
            bYear = studentInfo(6).ToString.Substring(0, 4)
            Dim birthday As New DateTime(CInt(bYear), CInt(bMonth), CInt(bDay))
            Dim firstOfYear As New DateTime(CInt(getSchoolYear()), 1, 1)
            age = Math.Floor((firstOfYear - birthday).Days / 365).ToString
            gradeEntry = "September " + Str(CInt(bYear + 14)) ' Looks like this code is too complcated  Str(DateTime.Now.Year - (Math.Floor((DateTime.Now - birthday).Days / 365) - 14))
            Dim columnInfo As New ArrayList From {lastName, firstName, gender, age, bDay, bMonth, bYear, gradeEntry}

            Dim pastColumnWidths As Integer = 0
            Dim centeredX As Integer
            For col As Integer = 0 To columnInfo.Count - 1
                font = New Font("Arial", defaultFontSize)
                centeredX = getCenteredX(columnWidths(col) / scalingFactor, columnInfo(col), font, e)

                e.Graphics.DrawString(columnInfo(col), font, Brushes.Black, startX + pastColumnWidths + centeredX, startY + columnHeight * row)

                pastColumnWidths = pastColumnWidths + columnWidths(col) / scalingFactor
            Next

            studentsToPrint.RemoveAt(0)
        Next

    End Sub
    Private Function getSchoolYear() As String
        Dim checkdate As New Date(2014, 7, 1)
        If Date.Now.Month < checkdate.Month Then
            Return Str(Date.Now.Year - 1)
        Else
            Return Date.Now.Year.ToString
        End If
    End Function
    Private Function getCenteredX(ByVal totalSpace As Integer, ByVal text As String, ByRef font As Font, ByVal e As Printing.PrintPageEventArgs) As Integer

        While e.Graphics.MeasureString(text, font).Width >= totalSpace
            font = New Font("Arial", font.Size - 1)
        End While

        Return (totalSpace - e.Graphics.MeasureString(text, font).Width) / 2


    End Function

    Private Function getAppropiateFont(ByVal defaultFont As Font, ByVal text As String, ByVal maxWidth As Integer, ByVal e As Printing.PrintPageEventArgs) As Font
        While e.Graphics.MeasureString(text, defaultFont).Width >= maxWidth
            defaultFont = New Font(defaultFont.FontFamily, defaultFont.Size - 1)
        End While
        Return defaultFont
    End Function
    Private Sub PreviewEligForm(Optional ByVal justPrint = False)

        If Not justPrint Then 'If the students havent been selected yet

            If clubNameTxt.Visible = True Then
                MsgBox("You must exit edit mode before you print or preview a document.")
                Exit Sub
            End If


            'Displaying student selection screen
            PrintSelection.currentStudents = currentStudents.Clone()
            PrintSelection.refreshListview()
            PrintSelection.printBtn.Text = "Preview"
            PrintSelection.ShowDialog()
            studentsToPrint = PrintSelection.selectedStudents

            If studentsToPrint(0).Equals("cancel") Then
                studentsToPrint.Clear()
                Exit Sub
            End If

        End If
        'newPrintJob = True
        PrintPreviewDialog1.Document = EligForm
        EligForm.DocumentName = clubNameLbl.Text + " Eligibility Form"

        PrintPreviewDialog1.WindowState = FormWindowState.Maximized
        MsgBox("This window is simply a preview, printing from this window will not work.", MsgBoxStyle.Information, "Reminder")
        PrintPreviewDialog1.ShowDialog()

    End Sub
    Private Sub PrintEligForm(Optional ByVal justPrint = False)
        If Not justPrint Then 'If the students havent been selected yet

            If clubNameTxt.Visible = True Then
                MsgBox("You must exit edit mode before you print or preview a document.")
                Exit Sub
            End If



            'Displaying student selection screen
            PrintSelection.currentStudents = currentStudents.Clone()
            PrintSelection.refreshListview()
            PrintSelection.printBtn.Text = "Print"
            PrintSelection.ShowDialog()
            studentsToPrint = PrintSelection.selectedStudents

            If studentsToPrint(0).Equals("cancel") Then
                studentsToPrint.Clear()
                Exit Sub
            End If

        End If

        PrintDialog1.Document = EligForm
        EligForm.DocumentName = clubNameLbl.Text + " Eligibility Form"
        If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            EligForm.Print()
        End If
    End Sub
    Private Sub PreviewAttendanceList(Optional ByVal justPrint = False)

        If Not justPrint Then 'If the students havent been selected yet

            If clubNameTxt.Visible = True Then
                MsgBox("You must exit edit mode before you print or preview a document.")
                Exit Sub
            End If

            'Displaying student selection screen
            PrintSelection.currentStudents = currentStudents.Clone()
            PrintSelection.refreshListview()
            PrintSelection.printBtn.Text = "Preview"
            PrintSelection.ShowDialog()
            studentsToPrint = PrintSelection.selectedStudents

            If studentsToPrint(0).Equals("cancel") Then
                studentsToPrint.Clear()
                Exit Sub
            End If

        End If
        newPrintJob = True
        PrintPreviewDialog1.Document = NameList
        NameList.DocumentName = clubNameLbl.Text + " Attendance List"

        PrintPreviewDialog1.WindowState = FormWindowState.Maximized
        MsgBox("This window is simply a preview, printing from this window will not work.", MsgBoxStyle.Information, "Reminder")
        PrintPreviewDialog1.ShowDialog()

    End Sub

    Private Sub PreviewPhotoGrid(Optional ByVal justPrint = False)
        If Not justPrint Then 'If the students havent been selected yet

            If clubNameTxt.Visible = True Then
                MsgBox("You must exit edit mode before you print or preview a document.")
                Exit Sub
            End If

            'Displaying student selection screen
            PrintSelection.currentStudents = currentStudents.Clone()
            PrintSelection.refreshListview()
            PrintSelection.printBtn.Text = "Preview"
            PrintSelection.ShowDialog()
            studentsToPrint = PrintSelection.selectedStudents

            If studentsToPrint(0).Equals("cancel") Then
                studentsToPrint.Clear()
                Exit Sub
            End If

        End If
        newPrintJob = True
        PrintPreviewDialog1.Document = PhotoGrid
        PhotoGrid.DocumentName = clubNameLbl.Text + " Photo Grid"
        PrintPreviewDialog1.WindowState = FormWindowState.Maximized
        MsgBox("This window is simply a preview, printing from this window will not work.", MsgBoxStyle.Information, "Reminder")
        PrintPreviewDialog1.ShowDialog()

    End Sub

    Private Sub PrintPhotoGrid(Optional ByVal justPrint = False)
        If Not justPrint Then 'If the students havent been selected yet

            If clubNameTxt.Visible = True Then
                MsgBox("You must exit edit mode before you print or preview a document.")
                Exit Sub
            End If



            'Displaying student selection screen
            PrintSelection.currentStudents = currentStudents.Clone()
            PrintSelection.refreshListview()
            PrintSelection.printBtn.Text = "Print"
            PrintSelection.ShowDialog()
            studentsToPrint = PrintSelection.selectedStudents

            If studentsToPrint(0).Equals("cancel") Then
                studentsToPrint.Clear()
                Exit Sub
            End If

        End If
        newPrintJob = True
        PrintDialog1.Document = PhotoGrid
        PhotoGrid.DocumentName = clubNameLbl.Text + " Photo Grid"
        If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            PhotoGrid.Print()
        End If
    End Sub

    Private Sub PrintAttendanceList(Optional ByVal justPrint = False)

        If Not justPrint Then 'If the students havent been selected yet

            If clubNameTxt.Visible = True Then
                MsgBox("You must exit edit mode before you print or preview a document.")
                Exit Sub
            End If



            'Displaying student selection screen
            PrintSelection.currentStudents = currentStudents.Clone()
            PrintSelection.refreshListview()
            PrintSelection.printBtn.Text = "Print"
            PrintSelection.ShowDialog()
            studentsToPrint = PrintSelection.selectedStudents

            If studentsToPrint(0).Equals("cancel") Then
                studentsToPrint.Clear()
                Exit Sub
            End If

        End If
        newPrintJob = True
        PrintDialog1.Document = NameList
        NameList.DocumentName = clubNameLbl.Text + " Attendance List"
        If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            NameList.Print()
        End If
    End Sub

    Private Sub PrintStudentReport(Optional ByVal justPrint = False)

       
        newPrintJob = True
        PrintDialog1.Document = StudentReport
        StudentReport.DocumentName = "Student Report"
        If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            StudentReport.Print()
        End If
    End Sub

    Private Sub StudentReport_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles StudentReport.PrintPage
        Dim namesPerPage As Integer = 1

        'If its a new job then initialize the varibles
        If newPrintJob Then
            newPrintJob = False

            studentsToPrint.Clear()
            For Each studentNumber As String In studentsToReport
                studentsToPrint.Add(file.getNameFromNumber(studentNumber))
            Next
            pagesNeeded = Math.Ceiling(studentsToPrint.Count / namesPerPage)
            currentPage = 0
        End If

        If pagesNeeded > 1 Then
            printReport(e)
            e.HasMorePages = True
            studentsToPrint.RemoveRange(0, namesPerPage)
            studentsToReport.RemoveRange(0, namesPerPage)
            pagesNeeded = pagesNeeded - 1
            currentPage = currentPage + 1
        ElseIf pagesNeeded = 1 Then
            printReport(e)
            pagesNeeded = 0
            currentPage = 0
            studentsToPrint.Clear()
            e.HasMorePages = False
        End If

    End Sub
    Public Sub printReport(e As Printing.PrintPageEventArgs)
        Dim normalFont As New Font("Arial", 15)
        Dim titleFont As New Font("Arial", 25, FontStyle.Underline)
        Dim currentX As Integer = 0
        Dim currentY As Integer = 200
        Dim xSpacing As Integer = 0
        Dim ySpacing As Integer = 0
        Dim textHeight As Integer = e.Graphics.MeasureString("The Quick Brown fox jumped over the lazy dog", normalFont).Height
        Dim schoolYear As String = getSchoolYear() + "/" + CStr(CInt(getSchoolYear()) + 1)

        e.Graphics.DrawString("Student Extracurricular Report - " + schoolYear, titleFont, Brushes.Black, (e.MarginBounds.Width - e.Graphics.MeasureString("Student Extracurricular Report - " + schoolYear, titleFont).Width) / 2, currentY - 200) 'Drawing main title
        titleFont = New Font("Arial", 15, FontStyle.Underline)
        e.Graphics.DrawString(studentsToPrint(0), titleFont, Brushes.Black, (e.MarginBounds.Width - e.Graphics.MeasureString(studentsToPrint(0), titleFont).Width) / 2, currentY - 100) 'Drawing student name

 

        If reports(studentsToReport(0)).Keys.Count = 0 Then
            e.Graphics.DrawString("This student does not participate in any clubs.", normalFont, Brushes.Black, (e.MarginBounds.Width - e.Graphics.MeasureString("This student does not participate in any clubs.", normalFont).Width) / 2, currentY)
            Exit Sub
        End If

        For Each type As String In reports(studentsToReport(0)).Keys
            If currentX + getMaxWidthOfType(reports(studentsToReport(0)), type, titleFont, normalFont, e) > e.MarginBounds.Width Then 'If the new type is going to go off the page then move it to the next line
                currentX = 0
                currentY = currentY + ySpacing
                ySpacing = 0
            End If
            e.Graphics.DrawString(type, titleFont, Brushes.Black, currentX, currentY) 'Drawing title
            Dim tempCurrentY As Integer = currentY + e.Graphics.MeasureString(type, titleFont).Height + 3
            For Each club As String In reports(studentsToReport(0))(type)              'Looping through clubs and drawing them
                e.Graphics.DrawString(club, normalFont, Brushes.Black, currentX, tempCurrentY)
                tempCurrentY = tempCurrentY + textHeight + 3
            Next
            If tempCurrentY - currentY > ySpacing Then ySpacing = tempCurrentY - currentY + 10 'Making the spacing bigger if it has to be 
            currentX = currentX + getMaxWidthOfType(reports(studentsToReport(0)), type, titleFont, normalFont, e) + 20
        Next

    End Sub
    Private Function getMaxWidthOfType(ByVal report As Dictionary(Of String, ArrayList), ByVal type As String, ByVal titlefont As Font, ByVal normalfont As Font, ByVal e As Printing.PrintPageEventArgs) As Integer
        Dim width As Integer = e.Graphics.MeasureString(type, titlefont).Width
        For Each club As String In report(type)
            If e.Graphics.MeasureString(club, normalfont).Width > width Then width = e.Graphics.MeasureString(club, normalfont).Width
        Next
        Return width
    End Function
    Private Sub PreviewBusManifest(Optional ByVal justPrint = False)
        If Not justPrint Then 'If the students havent been selected yet

            If clubNameTxt.Visible = True Then
                MsgBox("You must exit edit mode before you print or preview a document.")
                Exit Sub
            End If



            'Displaying student selection screen
            PrintSelection.currentStudents = currentStudents.Clone()
            PrintSelection.refreshListview()
            PrintSelection.printBtn.Text = "Preview"
            PrintSelection.ShowDialog()
            studentsToPrint = PrintSelection.selectedStudents

            If studentsToPrint(0).Equals("cancel") Then
                studentsToPrint.Clear()
                Exit Sub
            End If

        End If

        PrintPreviewDialog1.Document = BusManifest
        BusManifest.DocumentName = clubNameLbl.Text + " Student Manifest"
        PrintPreviewDialog1.WindowState = FormWindowState.Maximized

        newPrintJob = True
        getStudentManifestInfo()
        MsgBox("This window is simply a preview, printing from this window will not work.", MsgBoxStyle.Information, "Reminder")
        PrintPreviewDialog1.ShowDialog()

    End Sub
    Private Sub PrintBusManifest(Optional ByVal justPrint As Boolean = False)
        If Not justPrint Then 'If the students havent been selected yet

            If clubNameTxt.Visible = True Then
                MsgBox("You must exit edit mode before you print or preview a document.")
                Exit Sub
            End If

            'Displaying student selection screen
            PrintSelection.currentStudents = currentStudents.Clone()
            PrintSelection.refreshListview()
            PrintSelection.printBtn.Text = "Print"
            PrintSelection.ShowDialog()
            studentsToPrint = PrintSelection.selectedStudents

            If studentsToPrint(0).Equals("cancel") Then
                studentsToPrint.Clear()
                Exit Sub
            End If

        End If

        PrintDialog1.Document = BusManifest
        BusManifest.DocumentName = clubNameLbl.Text + " Student Manifest"
        If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            newPrintJob = True
            getStudentManifestInfo()
            BusManifest.Print()
        End If
    End Sub
    Private Sub getStudentManifestInfo()
        studentManifestInfo.Clear()
        StudentManifestForm.sportsName = clubNameLbl.Text
        StudentManifestForm.teacher = currentUser
        StudentManifestForm.ShowDialog()
        studentManifestInfo = StudentManifestForm.results.Clone
    End Sub
    Private Sub getStudentsToPrint()
        If clubNameTxt.Visible = True Then
            MsgBox("You must exit edit mode before you print or preview a document.")
            Exit Sub
        End If

        PrintDialog1.Document = BusManifest
        BusManifest.DocumentName = clubNameLbl.Text + " Student Manifest"

        'Displaying student selection screen
        CreateSelection.currentStudents = currentStudents.Clone()
        CreateSelection.refreshListview()
        CreateSelection.selectAll()
        CreateSelection.ShowDialog()
        studentsToPrint = CreateSelection.selectedStudents


    End Sub

    Private Sub AttendanceListToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AttendanceListToolStripMenuItem.Click
        getStudentsToPrint()
        If CreateSelection.DialogResult = Windows.Forms.DialogResult.Cancel Or CreateSelection.DialogResult = Windows.Forms.DialogResult.None Then
            studentsToPrint.Clear()
            Return
        ElseIf CreateSelection.DialogResult = Windows.Forms.DialogResult.Yes Then
            PrintAttendanceList(True)
        ElseIf CreateSelection.DialogResult = Windows.Forms.DialogResult.OK Then
            PreviewAttendanceList(True)
        End If

    End Sub

    Private Sub BusManifestToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BusManifestToolStripMenuItem.Click
        getStudentsToPrint()
        If CreateSelection.DialogResult = Windows.Forms.DialogResult.Cancel Or CreateSelection.DialogResult = Windows.Forms.DialogResult.None Then
            studentsToPrint.Clear()
            Return
        ElseIf CreateSelection.DialogResult = Windows.Forms.DialogResult.Yes Then
            PrintBusManifest(True)
        ElseIf CreateSelection.DialogResult = Windows.Forms.DialogResult.OK Then
            PreviewBusManifest(True)
        End If
    End Sub

    Private Sub ExcelDocumentToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExcelDocumentToolStripMenuItem.Click
        ExportToExcelDocument()
    End Sub

    Private Sub PhotoGridToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PhotoGridToolStripMenuItem.Click
        getStudentsToPrint()
        If CreateSelection.DialogResult = Windows.Forms.DialogResult.Cancel Or CreateSelection.DialogResult = Windows.Forms.DialogResult.None Then
            studentsToPrint.Clear()
            Return
        ElseIf CreateSelection.DialogResult = Windows.Forms.DialogResult.Yes Then
            printPhotoGrid(True)
        ElseIf CreateSelection.DialogResult = Windows.Forms.DialogResult.OK Then
            PreviewPhotoGrid(True)
        End If
    End Sub

#End Region

#Region "InfoPanel Code"

    Private Sub infoPanelTimer_Tick(sender As Object, e As EventArgs) Handles infoPanelAnimationTimer.Tick
        Dim multiplier As Integer = 0
        If infoPanelShown Then
            multiplier = -1
        Else
            multiplier = 1
        End If

        Me.Width = Me.Width + multiplier * aniSpeed

        If Me.Width <= startWidth Then
            infoPanelAnimationTimer.Enabled = False
            infoPanelShown = False
            infoForcedWidth = infoTargetWidth

        ElseIf Me.Width >= infoForcedWidth Then
            infoPanelAnimationTimer.Enabled = False
            infoPanelShown = True
            showInfo()

        End If
    End Sub


    Private Sub showInfo()
        If membersList.SelectedItems.Count = 0 Or membersList.SelectedItem.ToString = "There are no members in this club." Then
            Exit Sub
        End If

        Dim studentNumber As String = currentStudents(membersList.SelectedIndex)
        Dim imagePath As String = picturesDirectory + studentNumber + ".bmp"

        If My.Computer.FileSystem.FileExists(imagePath) = False Then
            imagePath = My.Computer.FileSystem.CurrentDirectory + "/NotFound.bmp"
        End If
        studentPicture.ImageLocation = imagePath
        'Getting student number and name

        lastLbl.Text = file.getAttendanceNameFromNumber(studentNumber).Split(",").GetValue(0)
        firstLbl.Text = file.getAttendanceNameFromNumber(studentNumber).Split(",").GetValue(1)
        numberLbl.Text = studentNumber

        'Getting the rest of the info
        Dim infoList As New ArrayList
        infoList = file.numberDictionary(studentNumber).Clone
        Dim birthText As String = infoList(6).ToString.Substring(0, 4) + "/" + infoList(6).ToString.Substring(4, 2) + "/" + infoList(6).ToString.Substring(6, 2)
        Dim birthday As New DateTime(CInt(birthText.Split("/").GetValue(0)), CInt(birthText.Split("/").GetValue(1)), CInt(birthText.Split("/").GetValue(2)))
        bornLbl.Text = Math.Floor((DateTime.Now - birthday).Days / 365).ToString + "(" + birthText + ")"

        stuTeleLbl.Text = checkEmpty(infoList(7))
        guardTeleLbl.Text = checkEmpty(infoList(8))
        stuEmailLbl.Text = checkEmpty(infoList(9))

        parentEmail1Lbl.Text = ""
        parentEmail2Lbl.Text = ""

        'Adding the emails
        Dim rawEmails As String = checkEmpty(infoList(10))
        If rawEmails.Contains(";") Then
            'Formatting the first email(making it say Joe:joe@gmail instead of joe<joe@gmail>)
            Dim firstEmail As String = rawEmails.Split(";").GetValue(0).ToString.Replace("<", ": ")
            parentEmail1Lbl.Text = firstEmail.Substring(0, firstEmail.Count - 1)

            'Formatting the second email
            Dim secondEmail As String = rawEmails.Split(";").GetValue(1)
            If rawEmails.Length > firstEmail.Length + 1 And secondEmail.First = " " Then secondEmail = secondEmail.Substring(1)
            secondEmail = secondEmail.Replace("<", ": ")
            parentEmail2Lbl.Text = secondEmail.Substring(0, secondEmail.Count - 1)

        Else
            rawEmails = rawEmails.ToString.Replace("<", ": ")
            parentEmail1Lbl.Text = rawEmails.Substring(0, rawEmails.Count - 1)
        End If

        'Making the form wider if the email if too long for the screen
        While Me.Width - 20 < parentEmail1Lbl.Location.X + parentEmail1Lbl.Width Or Me.Width - 20 < parentEmail2Lbl.Location.X + parentEmail2Lbl.Width
            Me.Width = Me.Width + 1
        End While
        infoForcedWidth = Me.Width

        'If getNote(studentNumber) = "" Then
        '    noteLbl.Text = "Double click to add note about this student."
        'Else
        '    noteLbl.Text = getNote(studentNumber)
        'End If

    End Sub

    Private Sub CheckboxTimer_Tick(sender As Object, e As EventArgs) Handles CheckboxAnimationTimer.Tick
        If detailsShown Then
            membersList.Height = membersList.Height + 1

            If membersList.Location.Y + membersList.Height > infoCheckbox.Location.Y + infoCheckbox.Height Then
                detailsShown = False
                CheckboxAnimationTimer.Enabled = False
            End If

        Else
            membersList.Height = membersList.Height - 1

            If membersList.Location.Y + membersList.Height < infoCheckbox.Location.Y Then
                detailsShown = True
                CheckboxAnimationTimer.Enabled = False
            End If
        End If
    End Sub

    Private Sub membersList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles membersList.SelectedIndexChanged

        If detailsShown = False And membersList.SelectedItems.Count > 0 Then
            CheckboxAnimationTimer.Enabled = True
        ElseIf detailsShown And membersList.SelectedItems.Count = 0 Then
            CheckboxAnimationTimer.Enabled = True
        End If
        If infoPanelShown Then showInfo()
    End Sub

    Private Sub infoCheckbox_CheckedChanged(sender As Object, e As EventArgs) Handles infoCheckbox.CheckedChanged
        infoPanelAnimationTimer.Enabled = True
    End Sub

    Private Sub firstLbl_Click(sender As Object, e As EventArgs) Handles firstLbl.Click
        My.Computer.Clipboard.SetText(firstLbl.Text)
        MsgBox("'" + firstLbl.Text + "' was copied to clipboard")
    End Sub

    Private Sub lastLbl_Click(sender As Object, e As EventArgs) Handles lastLbl.Click
        My.Computer.Clipboard.SetText(lastLbl.Text)
        MsgBox("'" + lastLbl.Text + "' was copied to clipboard")
    End Sub

    Private Sub numberLbl_Click(sender As Object, e As EventArgs) Handles numberLbl.Click
        My.Computer.Clipboard.SetText(numberLbl.Text)
        MsgBox("'" + numberLbl.Text + "' was copied to clipboard")
    End Sub

    Private Sub stuTeleLbl_Click(sender As Object, e As EventArgs) Handles stuTeleLbl.Click
        My.Computer.Clipboard.SetText(stuTeleLbl.Text)
        MsgBox("'" + stuTeleLbl.Text + "' was copied to clipboard")
    End Sub

    Private Sub guardTeleLbl_Click(sender As Object, e As EventArgs) Handles guardTeleLbl.Click
        My.Computer.Clipboard.SetText(guardTeleLbl.Text)
        MsgBox("'" + guardTeleLbl.Text + "' was copied to clipboard")
    End Sub

    Private Sub stuEmailLbl_Click(sender As Object, e As EventArgs) Handles stuEmailLbl.Click
        If Not stuEmailLbl.Text = "None" Then
            My.Computer.Clipboard.SetText(stuEmailLbl.Text)
            MsgBox("'" + stuEmailLbl.Text + "' was copied to clipboard")
        End If
    End Sub

    Private Sub parentEmail1Lbl_Click(sender As Object, e As EventArgs) Handles parentEmail1Lbl.Click
        If Not parentEmail1Lbl.Text = "None" Then
            My.Computer.Clipboard.SetText(parentEmail1Lbl.Text.Split(":").GetValue(1).ToString.Substring(1))
            MsgBox("'" + parentEmail1Lbl.Text.Split(":").GetValue(1).ToString.Substring(1) + "' was copied to clipboard")
        End If
    End Sub

    Private Sub parentEmail2Lbl_Click(sender As Object, e As EventArgs) Handles parentEmail2Lbl.Click
        If Not parentEmail1Lbl.Text = "" Then
            My.Computer.Clipboard.SetText(parentEmail2Lbl.Text.Split(":").GetValue(1).ToString.Substring(1))
            MsgBox("'" + parentEmail2Lbl.Text.Split(":").GetValue(1).ToString.Substring(1) + "' was copied to clipboard")
        End If
    End Sub

    'Private Sub noteLbl_DoubleClick(sender As Object, e As EventArgs)
    '    If clubNameLbl.Text = "nclb" Then
    '        MsgBox("You must save the club before adding notes.")
    '        Exit Sub
    '    End If
    '    noteLbl.Visible = False
    '    If notes.ContainsKey(numberLbl.Text) Then
    '        noteTxt.BringToFront()
    '        noteTxt.Text = notes(numberLbl.Text)
    '        noteTxt.Visible = True
    '    Else
    '        noteTxt.BringToFront()
    '        noteTxt.Text = "Enter Note"
    '        noteTxt.Visible = True
    '    End If
    'End Sub
    'Private Sub noteTxt_GotFocus(sender As Object, e As EventArgs)
    '    If noteTxt.Text = "Enter Note" Then noteTxt.Text = ""
    'End Sub

    'Private Sub noteTxt_KeyDown(sender As Object, e As KeyEventArgs)

    '    If e.KeyCode = Keys.Enter Then
    '        If Not file.isValidInput(noteTxt.Text, "|", "Enter Note") Then
    '            MsgBox("That is not a valid note.")
    '            Exit Sub
    '        End If
    '        notes.Add(numberLbl.Text, noteTxt.Text)
    '        noteTxt.Visible = False
    '        noteLbl.Visible = True
    '        noteLbl.Text = noteTxt.Text
    '        saveChanges(True)
    '        updateMembersList()
    '    End If
    'End Sub

#End Region

    Private Sub ExportToExcelDocument()
        Dim appXL As Excel.Application
        Dim wbXl As Excel.Workbook
        Dim shXL As Excel.Worksheet
        Dim raXL As Excel.Range

        ' Start Excel and get Application object.
        appXL = CreateObject("Excel.Application")
        appXL.Visible = True
        ' Add a new workbook.
        wbXl = appXL.Workbooks.Add()

        shXL = wbXl.ActiveSheet
        shXL.Name = clubNameLbl.Text

        ' Add table headers going cell by cell.
        shXL.Cells(1, 1).Value = "First Name"
        shXL.Cells(1, 2).Value = "Last Name"
        shXL.Cells(1, 3).Value = "Telephone 1"
        shXL.Cells(1, 4).Value = "Telephone 2"
        shXL.Cells(1, 5).Value = "Email 1"
        shXL.Cells(1, 6).Value = "Email 2"

        ' Format A1:D1 as bold, vertical alignment = center.
        With shXL.Range("A1", "F1")
            .Font.Bold = True
            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
        End With

        ' Create an array to set multiple values at once.
        Dim students(currentStudents.Count, 6) As String
        For i As Integer = 0 To currentStudents.Count - 1

            students(i, 0) = file.getAttendanceNameFromNumber(currentStudents(i)).Split(",").GetValue(1)
            students(i, 1) = file.getAttendanceNameFromNumber(currentStudents(i)).Split(",").GetValue(0)

            Dim infoList As ArrayList = file.numberDictionary(currentStudents(i)).Clone
            students(i, 2) = checkEmpty(infoList(7))
            students(i, 3) = checkEmpty(infoList(8))
            students(i, 4) = checkEmpty(IIf(infoList(10).ToString.Contains(";"), infoList(10).ToString.Split(";").GetValue(0), infoList(10)))
            Try
                students(i, 5) = checkEmpty(IIf(infoList(10).ToString.Contains(";"), infoList(10).ToString.Split(";").GetValue(1), "None"))
            Catch ex As Exception
                students(i, 5) = "None"
            End Try
        Next
        shXL.Range(shXL.Cells(2, 1), shXL.Cells(currentStudents.Count + 1, 6)).Value = students
        ' AutoFit columns A:D.
        raXL = shXL.Range("A1", "F1")
        raXL.EntireColumn.AutoFit()

        ' Make sure Excel is visible and give the user control
        ' of Excel's lifetime.
        appXL.Visible = True
        appXL.UserControl = True
        ' Release object references.
        raXL = Nothing
        shXL = Nothing
        wbXl = Nothing
        appXL.Quit()
        appXL = Nothing
        Exit Sub
Err_Handler:
        MsgBox(Err.Description, vbCritical, "Error: " & Err.Number)

    End Sub

#Region "Command Code"

    Private Sub parseCommand(sender As Object, e As KeyEventArgs) Handles commandTxtbox.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim inputString As String = file.capitalize(commandTxtbox.Text).Trim(" ")
            commandErrorLabel.Visible = False
            Dim commandLength As Integer = 0

            'Error handling   
            If isVaidCommand(inputString, commandLength) = False Then
                commandShakeTimer.Enabled = True
                commandErrorLabel.Text = "Invalid command. Type 'help' for valid commands."
                Exit Sub
            ElseIf inputString.Split(" ").Length = commandLength Then
                commandShakeTimer.Enabled = True
                commandErrorLabel.Text = "Incorrect usage. Type 'help' for the correct usage."
                Exit Sub
            End If

            'Parsing input (it is considered valid if it got this far)
            Dim keyword As String = ""
            Dim clubName As String = ""
            Dim splitInput = inputString.Split(" ")

            'Extracting keyword and clubname from input
            For i As Integer = 0 To splitInput.Length - 1
                If i < commandLength Then
                    keyword = keyword + splitInput.GetValue(i) + " "
                Else
                    clubName = clubName + splitInput.GetValue(i) + " "
                End If

            Next
            keyword = keyword.Trim(" ")
            clubName = clubName.Trim(" ")


            If clubWithName(clubName) = False And Not keyword = "Add" Then
                commandShakeTimer.Enabled = True
                commandErrorLabel.Text = "Invalid club name. Use a valid club name."
            End If
            If Not keyword = "Add" Then
                ListViewClubs.SelectedItems.Clear()
                ListViewClubs.FindItemWithText(clubName).Selected = True
            End If
            Select Case keyword
                Case "Delete"
                    deleteBtn.Visible = True
                    deleteBtn.PerformClick()
                Case "Edit"
                    editSaveBtn.PerformClick()
                Case "Print Attendance List"
                    PrintAttendanceList()
                Case "Preview Attendance List"
                    PreviewAttendanceList()
                Case "Print Student Manifest"
                    PrintBusManifest()
                Case "Preview Student Manifest"
                    PreviewBusManifest()
                Case "Print Photo Grid"
                    printPhotoGrid()
                Case "Preview Photo Grid"
                    PreviewPhotoGrid()
                Case "Add"
                    addClubBtn.PerformClick()
                    clubNameTxt.Text = clubName
                    editSaveBtn.PerformClick()
            End Select
            commandTxtbox.Text = ""
            commandMoveTimer.Enabled = True
        End If
    End Sub
    Private Function clubWithName(ByVal clubName As String)
        For Each item As ListViewItem In ListViewClubs.Items
            If clubName.ToLower = item.Text.ToLower Then
                Return True
            End If
        Next
        Return False
    End Function
    Public Function isVaidCommand(ByVal inputString As String, ByRef commandLength As Integer)
        Dim splitString = inputString.Split(" ")

        'If its a valid command then return true
        If keywords.Contains(splitString.GetValue(0)) Then
            commandLength = 1
            Return True

            'If the first word is not a valid command and its is not long enough to form one of the longer commands (ex. print attendance list)
        ElseIf keywords.Contains(splitString.GetValue(0)) = False And splitString.Length < 3 Then
            Return False

            'If it is one of the 3 work commands
        ElseIf keywords.Contains(splitString.GetValue(0) + " " + splitString.GetValue(1) + " " + splitString.GetValue(2)) Then
            commandLength = 3
            Return True
        End If

        'If it gets here then its an invalid command
        Return False

    End Function

    Private Sub commandShakeTimer_Tick(sender As Object, e As EventArgs) Handles commandShakeTimer.Tick
        If shakeProgress = shakeDuration Then 'If its done shaking
            commandTxtbox.Location = New Point(commandTxtbox.Location.X - shakeDistance, commandTxtbox.Location.Y)
            shakeProgress = 0
            commandShakeTimer.Enabled = False

            Exit Sub
        End If

        If shakeProgress = 0 Then 'If its the first shake
            commandTxtbox.Location = New Point(commandTxtbox.Location.X - shakeDistance, commandTxtbox.Location.Y)
            commandErrorTimer.Enabled = True
            commandErrorLabel.Visible = True
        Else                                                              'Determining which direction to move the textbox based on if the direction count is even or odd 
            commandTxtbox.Location = New Point(commandTxtbox.Location.X + IIf((shakeProgress / commandShakeTimer.Interval) Mod 2 = 0, -2 * shakeDistance, 2 * shakeDistance), commandTxtbox.Location.Y)
        End If

        shakeProgress = shakeProgress + commandShakeTimer.Interval
    End Sub

    Private Sub commandErrorTimer_Tick(sender As Object, e As EventArgs) Handles commandErrorTimer.Tick
        commandErrorLabel.Visible = False
        commandErrorTimer.Enabled = False
    End Sub

    Private Sub commandMoveTimer_Tick(sender As Object, e As EventArgs) Handles commandMoveTimer.Tick

        If moveDirection = 0 Then
            commandTxtbox.Text = "Enter command or type 'help' for list of commands."
            If commandTxtbox.Location.Y > 0 Then
                moveDirection = -1
            Else
                moveDirection = 1
                commandTxtbox.Enabled = True
                'Else
                '    commandMoveTimer.Enabled = False
            End If

        End If

        commandTxtbox.Location = New Point(commandTxtbox.Location.X, commandTxtbox.Location.Y + moveDirection * moveDistance * IIf(moveDirection < 0, 2, 1))
        commandErrorLabel.Location = New Point(commandErrorLabel.Location.X, commandErrorLabel.Location.Y + moveDirection * moveDistance * IIf(moveDirection < 0, 2, 1))
        commandErrorLabel.Visible = False

        If moveDirection = -1 And commandTxtbox.Location.Y <= -commandTxtbox.Height - moveDistance Then
            moveDirection = 0
            commandTxtbox.Enabled = False
            commandMoveTimer.Enabled = False

        ElseIf moveDirection = 1 And commandTxtbox.Location.Y >= moveTarget Then
            moveDirection = 0
            commandTxtbox.Focus()
            commandMoveTimer.Enabled = False
            commandTxtbox.SelectAll()
        End If

    End Sub

    Private Sub commandTxtbox_LostFocus(sender As Object, e As EventArgs) Handles commandTxtbox.LostFocus
        If commandTxtbox.Location.Y > 0 Then
            commandMoveTimer.Enabled = True
        End If
    End Sub

    Private Sub commandTxtbox_TextChanged(sender As Object, e As EventArgs) Handles commandTxtbox.TextChanged
        If commandTxtbox.Text.Contains("Enter command or type 'help' for list of commands.") And commandMoveTimer.Enabled = False Then
            commandTxtbox.Text = commandTxtbox.Text.Replace("Enter command or type 'help' for list of commands.", "")
        End If
    End Sub

#End Region


    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub


    Private Sub MailingListToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MailingListToolStripMenuItem.Click
        Dim mailingList As New ArrayList

        For Each studentNumber As String In currentStudents
            Dim infolist As New ArrayList
            infolist = file.numberDictionary(studentNumber).Clone
            Dim firstEmail As String = ""
            Dim secondEmail As String = ""

            'Extracting the emails
            Dim rawEmails As String = checkEmpty(infolist(10))

            If rawEmails.Contains(";") Then
                'Formatting the first email(making it say Joe:joe@gmail instead of joe<joe@gmail>)
                firstEmail = rawEmails.Split(";").GetValue(0).ToString.Replace("<", ": ")

                'Formatting the second email
                secondEmail = rawEmails.Split(";").GetValue(1)
                If rawEmails.Length > firstEmail.Length + 1 And secondEmail.First = " " Then secondEmail = secondEmail.Substring(1)
                secondEmail = secondEmail.Replace("<", ": ")

            Else
                rawEmails = rawEmails.ToString.Replace("<", ": ")
                firstEmail = rawEmails.Substring(0, rawEmails.Count - 1)
            End If

            If firstEmail.Contains(":") Then
                mailingList.Add(firstEmail.Split(":").GetValue(1).ToString.Replace(">", ""))
            End If

            If secondEmail.Contains(":") Then
                mailingList.Add(secondEmail.Split(":").GetValue(1).ToString.Replace(">", ""))
            End If
        Next

        Dim finalString As String = ""
        For Each email As String In mailingList
            finalString = finalString + email + ";"
        Next

        My.Computer.Clipboard.SetText(finalString)
        'Change prompt to
        ' Mailing List has been copied to your clipboard.
        'You can now paste it into an e-mail or create a mailing list.

        'To create a mailing list:
        '- Go into CHATT. 
        '- Select "New" then "Mailing List". 
        '- Type in the name of the group (this is what you will type when you want to send an e-mail to the group)
        '- In the Members field, right-click then select "Paste"


        Dim prompt As String = "Mailing List has been copied to your clipboard ! You can now paste it into an e-mail or create a mailing list." + vbNewLine + vbNewLine _
                               + "To create a mailing list:" + vbNewLine + "    - Go into CHATT" + vbNewLine _
                               + "  - Select 'New' then 'Mailing List'" + vbNewLine _
                               + "  - Type in the name of the group (this is what you will type when you want to send an e-mail to the group)" + vbNewLine _
                               + "- In the Members field, right-click then select 'Paste'"
        MsgBox(prompt)
    End Sub

    Private Sub EligibilityFormToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EligibilityFormToolStripMenuItem.Click
        getStudentsToPrint()
        If CreateSelection.DialogResult = Windows.Forms.DialogResult.Cancel Or CreateSelection.DialogResult = Windows.Forms.DialogResult.None Then
            studentsToPrint.Clear()
            Return
        ElseIf CreateSelection.DialogResult = Windows.Forms.DialogResult.Yes Then
            PrintEligForm(True)
        ElseIf CreateSelection.DialogResult = Windows.Forms.DialogResult.OK Then
            PreviewEligForm(True)
        End If
    End Sub

    Private Sub CreateMasterListToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CreateMasterListToolStripMenuItem.Click
       

        MasterListSelector.fillListview(types)
        MasterListSelector.ShowDialog()
        If MasterListSelector.DialogResult = Windows.Forms.DialogResult.Cancel Then Exit Sub 'The user pressed cancel in the selection menu
        Dim accounts As New ArrayList
        accounts = LoginScreen.getAccountNames()

        Dim validTypes As New ArrayList
        validTypes = MasterListSelector.returnValue.Clone()

        Dim students As New Dictionary(Of String, ArrayList)
        Dim maxClubsPerStudent As Integer = 0
        Dim clubNames As New ArrayList
        For Each account As String In accounts ' Looping through all of the accounts 

            Dim clubName As String = Dir(account + "/")
            While Not clubName = "" 'Looping until all clubs in this account have been checked

                Dim reader As New System.IO.StreamReader(account + "/" + clubName) 'Reading the files
                Dim type As String = reader.ReadLine
                If validType(type) = False Then ' If the type is invalid skip it and tell the user that you skipped it
                    MsgBox("The club " + clubName.Replace(".txt", "") + " from " + account + "'s account does not have a valid type so it was skipped.")
                    clubName = Dir() 'Getting next club
                    Continue While
                End If
                If validTypes.Contains(type) = False Then ' If the type is valid but it doesnt match the filter skip it
                    clubName = Dir() 'Getting next club
                    Continue While
                End If
                Dim currentLine As String = reader.ReadLine

                Dim clubMembers As New ArrayList
                clubMembers.Add(currentLine.Split("|").GetValue(0))
                While reader.EndOfStream = False
                    currentLine = reader.ReadLine 'Getting next line in file
                    clubMembers.Add(currentLine.Split("|").GetValue(0))


                End While
                reader.Close() 'Closing reader 

                Dim clubNameFormat = clubName.Replace(".txt", "") + " (" + account + ")"
                'If students are in club then add club name, else add a space
                For Each studentNumber As String In students.Keys
                    If clubMembers.Contains(studentNumber) Then
                        students(studentNumber).Add(clubNameFormat)
                        clubMembers.Remove(studentNumber)
                    Else
                        students(studentNumber).Add(" ")
                    End If
                Next

                'Adding new students to 'students' dictionary
                For Each newStudentNumber As String In clubMembers
                    Dim array As New ArrayList
                    For i As Integer = 1 To maxClubsPerStudent
                        array.Add(" ")
                    Next
                    array.Add(clubNameFormat)
                    students.Add(newStudentNumber, array)
                Next
                clubNames.Add(clubNameFormat)
                clubName = Dir() 'Getting next club 
                maxClubsPerStudent = maxClubsPerStudent + 1
            End While

        Next

        If maxClubsPerStudent = 0 Then ' If no clubs were added to the list then abort
            MsgBox("There were no clubs found that matched the types specified. Master list cannot be created")
            Exit Sub
        End If

        'Determining which clubs need parantheses beside them
        Dim newClubNames As New ArrayList
        Dim multiplies As New ArrayList
        Dim nameConversion As New Dictionary(Of String, String)
        multiplies = getMultiplies(clubNames.Clone)
        For Each clubname As String In clubNames
            If multiplies.Contains(removeParantheses(clubname)) = False Then
                newClubNames.Add(removeParantheses(clubname))
                nameConversion.Add(clubname, removeParantheses(clubname))
            Else
                newClubNames.Add(clubname)
                nameConversion.Add(clubname, clubname)
            End If
        Next
        clubNames = newClubNames.Clone
        nameConversion.Add(" ", " ")
        'Converting transfering data to sting array 
        Dim excelLayout(students.Keys.Count + 1, maxClubsPerStudent + 1) As String
        'Adding column Names
        excelLayout(0, 0) = "Student Name"
        For index As Integer = 0 To clubNames.Count - 1
            excelLayout(0, index + 1) = clubNames(index)
        Next
        Dim y As Integer = 1

        'Alphabitizing list
        Dim alphabetizedList As New ArrayList
        For Each studentNumber As String In students.Keys
            alphabetizedList.Add(file.getAttendanceNameFromNumber(studentNumber))
        Next
        alphabetizedList.Sort()

        For Each studentName As String In alphabetizedList
            excelLayout(y, 0) = studentName
            Dim studentKey As String = file.getNumberFromAttendanceName(students.Keys, studentName)
            For i As Integer = 0 To students(studentKey).Count - 1
                excelLayout(y, i + 1) = nameConversion(students(studentKey)(i))
            Next
            y = y + 1
        Next

        createMasterListExcel(excelLayout, students.Keys.Count, maxClubsPerStudent)

    End Sub

    Private Function getMultiplies(ByVal list As ArrayList) As ArrayList
        Dim multi As New ArrayList
        Dim deleted As Integer = 0
        Dim arraySize As Integer = list.Count - 1
        Dim element As String = ""
        For x As Integer = 0 To arraySize
            element = list(0)
            If multi.Contains(removeParantheses(element)) Then
                list.Remove(element)

            Else
                list.Remove(element)
                If arrayContainsPartialMatch(list, removeParantheses(element)) Then
                    multi.Add(removeParantheses(element))
                End If


            End If
        Next
        Return multi

    End Function
    Private Function removeParantheses(ByVal str As String) As String

        Return str.Remove(str.IndexOf("(") - 1)
    End Function
    Private Function arrayContainsPartialMatch(ByVal array As ArrayList, ByVal searchTerm As String) As Boolean
        For Each elem As String In array
            If elem.Contains(searchTerm) Then
                Return True
            End If
        Next
        Return False
    End Function
    Private Sub createMasterListExcel(ByVal excelLayout As Array, ByVal height As Integer, ByVal maxWidth As Integer)
        Dim appXL As Excel.Application
        Dim wbXl As Excel.Workbook
        Dim shXL As Excel.Worksheet
        Dim raXL As Excel.Range

        ' Start Excel and get Application object.
        appXL = CreateObject("Excel.Application")
        appXL.Visible = True
        ' Add a new workbook.
        wbXl = appXL.Workbooks.Add()

        shXL = wbXl.ActiveSheet
        shXL.Name = "Master List"

        shXL.Range(shXL.Cells(1, 1), shXL.Cells(height + 1, maxWidth + 1)).Value = excelLayout

        With shXL.Range(shXL.Cells(1, 1), shXL.Cells(1, maxWidth + 1))
            .Font.Bold = True
            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
        End With

        ' AutoFit columns A:D.
        raXL = shXL.Range(shXL.Cells(1, 1), shXL.Cells(1, maxWidth + 1))
        raXL.EntireColumn.AutoFit()

        ' Make sure Excel is visible and give the user control
        ' of Excel's lifetime.
        appXL.Visible = True
        appXL.UserControl = True
        ' Release object references.
        raXL = Nothing
        shXL = Nothing
        wbXl = Nothing
        appXL.Quit()
        appXL = Nothing
        Exit Sub
Err_Handler:
        MsgBox(Err.Description, vbCritical, "Error: " & Err.Number)

    End Sub


    Private Sub ImportClubToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ImportClubToolStripMenuItem.Click
        OpenFileDialog1.FileName = ""
        OpenFileDialog1.CheckFileExists = True
        Dim result As DialogResult = OpenFileDialog1.ShowDialog()
        If result = Windows.Forms.DialogResult.Cancel Then Exit Sub

        For i As Integer = 0 To OpenFileDialog1.FileNames.Length - 1
            Dim pathFile As String = OpenFileDialog1.FileNames.GetValue(i)
            Dim filename As String = OpenFileDialog1.SafeFileNames.GetValue(i).ToString


            'If the file already exists
            If My.Computer.FileSystem.FileExists(workPath + filename) Then
                filename = InputBox("The filename '" + filename.Replace(".txt", "") + "' was already taken. Please choose another one")
                If filename = "" Then
                    Continue For
                End If
                If filename.Contains(".txt") = False Then
                    filename = filename + ".txt"
                End If
            End If

            'copying file
            My.Computer.FileSystem.CopyFile(pathFile, workPath + filename)

            'Adding pipe (|) to end of line
            Dim reader As New System.IO.StreamReader(workPath + filename)
            Dim line As String = reader.ReadLine
            Dim finalString As String = ""
            While (reader.EndOfStream = False)
                If Not line.Last = "|" Then
                    If finalString.Contains(line) = False Then
                        line = line + "|"
                    End If
                End If

                If line.Last = "|" Then ' Only adding line if it has a pipe at the end
                    finalString = finalString + line + vbNewLine
                End If
                line = reader.ReadLine
            End While
            reader.Close()

            Dim writer As New System.IO.StreamWriter(workPath + filename)
            writer.Write(finalString)
            writer.Close()
            fillClubsListview()
        Next


    End Sub

    Private Sub addClassBtn_Click(sender As Object, e As EventArgs) Handles addClassBtn.Click
        Dim courseCode As String = InputBox("Enter the class course code.", "Add Class").Replace("-", "").Replace(" ", "").ToUpper
        If courseCode.Length = 0 Or courseCode.Replace(" ", "").Replace("-", "") = "" Then Exit Sub

        If Not courseCode.Length = 8 Then
            MsgBox("That is not a valid course code!")
            Exit Sub
        End If

        Dim studentsInCourse As New ArrayList
        For Each studentNumber In file.numberDictionary.Keys
            If file.numberDictionary(studentNumber)(13).ToString.Contains(courseCode) Then
                studentsInCourse.Add(studentNumber)
            End If
        Next

        If studentsInCourse.Count = 0 Then
            MsgBox("No course with the name " + courseCode + " was found. ")
        End If

        If MsgBox("All " + studentsInCourse.Count.ToString + " students from " + courseCode + " will be added.", MsgBoxStyle.OkCancel) = MsgBoxResult.Ok Then
            currentStudents.AddRange(studentsInCourse)
            currentStudents = removeDuplicates(currentStudents)
            updateMembersList()
            madeChanges = True
        End If
    End Sub

    Private Sub typeSelector_SelectedIndexChanged(sender As Object, e As EventArgs) Handles typeSelector.SelectedIndexChanged
        madeChanges = True
    End Sub

    Private Sub CreateStudentReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CreateStudentReportToolStripMenuItem.Click
        StudentBrowser.Button1.Text = "Select Students"
        StudentBrowser.selectedStudents.Clear()
        StudentBrowser.layoutListView()
        StudentBrowser.Text = "Select students to create report"
        StudentBrowser.ShowDialog()

        If StudentBrowser.DialogResult = Windows.Forms.DialogResult.Cancel Then Exit Sub

        studentsToReport = StudentBrowser.selectedStudents.Clone

        Dim accounts As New ArrayList
        accounts = LoginScreen.getAccountNames()
        reports.Clear()
        For Each currentStudent As String In studentsToReport
            Dim report As New Dictionary(Of String, ArrayList)
            For Each t As String In types
                report.Add(t, New ArrayList) 'initalizing the report
            Next
            Dim clubNames As New ArrayList
            For Each account As String In accounts ' Looping through all of the accounts 

                Dim clubName As String = Dir(account + "/")
                While Not clubName = "" 'Looping until all clubs in this account have been checked
                    
                    Dim reader As New System.IO.StreamReader(account + "/" + clubName) 'Reading the files
                    Dim type As String = reader.ReadLine
        
                    If validType(type) = False Or type = "Class" Or type = "Personal Use" Then ' If the type is invalid skip it
                        reader.Close()
                        clubName = Dir() 'Getting next club
                        Continue While
                    End If

                    Dim currentLine As String = reader.ReadLine

                    Dim clubMembers As New ArrayList
                    If currentStudent = currentLine.Split("|").GetValue(0) Then 'if the student is in the club
                        report(type).Add(clubName.Replace(".txt", "") + " (" + account + ")")
                    Else
                        While reader.EndOfStream = False
                            currentLine = reader.ReadLine 'Getting next line in file

                            If currentStudent = currentLine.Split("|").GetValue(0) Then 'if the student is in the club
                                report(type).Add(clubName.Replace(".txt", "") + " (" + account + ")")
                                Exit While
                            End If

                        End While
                    End If

                    reader.Close()
                    clubName = Dir()
                End While
            Next
            'Cloning keys of the dictionary
            Dim keys As New ArrayList
            For Each k As String In report.Keys
                keys.Add(k)
            Next
            'Looping through and removing empty keys
            For Each tt As String In keys
                If report(tt).Count = 0 Then
                    report.Remove(tt)
                End If
            Next

            reports.Add(currentStudent, report)
        Next

        If True Then
            newPrintJob = True
            PrintPreviewDialog1.Document = StudentReport
            StudentReport.DocumentName = " Student Report"

            PrintPreviewDialog1.WindowState = FormWindowState.Maximized
            MsgBox("This window is simply a preview, printing from this window will not work.", MsgBoxStyle.Information, "Reminder")
            PrintPreviewDialog1.ShowDialog()
        Else
            newPrintJob = True
            PrintPreviewDialog1.Document = StudentReport
            StudentReport.DocumentName = " Student Report"
            If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                StudentReport.Print()
            End If
        End If


    End Sub

  
End Class